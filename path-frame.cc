/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#include <std.h>

#include "path-frame.h"
#include "path-line.h"
#include "tunnel.h"

const int START_INDEX = 1000000;

/*
 * path_frame class
 */

path_frame::path_frame(context_object *parent)
	: context_object(parent)
	, _hash_id(std::rand())
	, _location(0)
	, _index(0)
	, _next(nullptr)
	, _prev(nullptr)
{}

path_frame::~path_frame()
{ 
	cut_out(); 
}

void path_frame::insert_back(path_frame *other)
{
	other->_prev = this;
	other->_next = _next;

	if (_next)
		_next->_prev = other;

	_next = other;
}

void path_frame::insert_front(path_frame *other)
{
	other->_prev = _prev;
	other->_next = this;

	if (_prev)
		_prev->_next = other;

	_prev = other;
}

void path_frame::cut_out()
{
	if (_prev)
		_prev->_next = _next;

	if (_next)
		_next->_prev = _prev;

	_prev = nullptr;
	_next = nullptr;
}

bool path_frame::test_for_collision(glm::vec3 point, 
							   float margin,
							   glm::vec3 *normal) const
{
	// TODO
	return false;
}

int path_frame::get_hash_id() const
{ return _hash_id; }

float path_frame::get_location() const
{ return _location; }

int path_frame::get_index() const
{ return _index; }

void path_frame::set_location(float location)
{ _location = location; }

void path_frame::set_index(int index)
{ _index = index; }

void path_frame::set_matrix(const glm::mat4 &matrix)
{ _matrix = matrix; }

float path_frame::distance_to_point(glm::vec3 point) const
{
	glm::vec3 off = point - glm::vec3(get_matrix() * glm::vec4(0, 0, 0, 1));

	return sqrtf(
			off.x * off.x
		  + off.y * off.y
		  + off.z * off.z
	);
}

const path_frame *path_frame::closer_one(glm::vec3 point) const
{
	// TODO fuck out that square root in distance_to_point method
	float pdist = _prev ? _prev->distance_to_point(point) : 999;
	float ndist = _next ? _next->distance_to_point(point) : 999;
	float cdist = distance_to_point(point);

	return pdist < ndist ? 
		pdist < cdist ? _prev : this 
		:
		ndist < cdist ? _next : this;
}

glm::mat4 path_frame::get_matrix() const
{ return _matrix; }

path_frame *path_frame::get_next()
{ return _next; }

path_frame *path_frame::get_prev()
{ return _prev; }

const path_frame *path_frame::get_next() const
{ return _next; }

const path_frame *path_frame::get_prev() const
{ return _prev; }
