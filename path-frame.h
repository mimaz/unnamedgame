/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#ifndef __PATH_FRAME_H
#define __PATH_FRAME_H

#include "context-object.h"

class path_line;

class path_frame : public context_object
{
  public:
	path_frame(context_object *parent);
	~path_frame();

	path_frame(const path_frame &other) = delete;
	path_frame(path_frame &&other) = delete;

	void insert_back(path_frame *other);
	void insert_front(path_frame *other);
	void cut_out();

	bool test_for_collision(glm::vec3 point, 
							float margin = 0,
							glm::vec3 *normal = nullptr) const;

	void set_location(float location);
	void set_index(int index);
	void set_matrix(const glm::mat4 &matrix);

	float distance_to_point(glm::vec3 point) const;
	const path_frame *closer_one(glm::vec3 point) const;

	int get_hash_id() const;
	float get_location() const;
	int get_index() const;
	glm::mat4 get_matrix() const;

	path_frame *get_next();
	path_frame *get_prev();
	const path_frame *get_next() const;
	const path_frame *get_prev() const;

  private:
	const int _hash_id;
	float _location;
	int _index;
	glm::mat4 _matrix;
	path_frame *_next;
	path_frame *_prev;
};

#endif
