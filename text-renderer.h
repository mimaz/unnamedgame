/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#ifndef __TEXT_RENDERER_H
#define __TEXT_RENDERER_H

#include "enums.h"
#include "context-part.h"

class char_renderer;

class text_renderer : public context_part
{
  public:
	text_renderer(context *context);

	void set_surface_width(float width);
	void set_surface_height(float height);
	void set_text_size(float size);
	void set_line_width(float width);
	void set_max_line_width();
	void set_text_color(glm::vec4 color);
	void set_vertical_alignment(vertical_alignment align);
	void set_horizontal_alignment(horizontal_alignment align);
	void set_text(const std::string &text);

	void render();
	void render(const glm::mat4 &matrix);

	float get_surface_width() const;
	float get_surface_height() const;
	float get_text_size() const;
	float get_line_width() const;
	glm::vec4 get_text_color() const;
	vertical_alignment get_vertical_alignment() const;
	horizontal_alignment get_horizontal_alignment() const;
	std::string get_text() const;

  private:
	float _width;
	float _height;
	float _size;
	float _line_width;
	glm::vec4 _color;
	vertical_alignment _valign;
	horizontal_alignment _halign;
	std::string _text;
};

#endif
