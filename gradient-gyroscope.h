/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#ifndef __GRADIENT_GYROSCOPE_H
#define __GRADIENT_GYROSCOPE_H

class gradient_gyroscope
{
  public:
	gradient_gyroscope();

	void tick();

	virtual void rotate(float angle) = 0;
	virtual glm::vec3 get_axis() = 0;
	virtual glm::vec3 get_vector() = 0;
	virtual glm::vec3 get_target() = 0;

  private:
	float _momentum;
};

#endif
