/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#ifndef __LIGHT_BOX_H
#define __LIGHT_BOX_H

#include "stuff.h"
#include "enums.h"

class light_source;

class light_box : public stuff
{
  public:
	static const float radius;

	light_box(stuff_manager *parent);
	light_box(stuff_manager *parent, float location);
	~light_box();

	void draw() override;
	void on_active_changed(bool active) override;
	void on_model_matrix_changed(const glm::mat4 &matrix) override;

	void set_color(rgb_color color);

	const light_source *get_light() const;
	rgb_color get_color() const;
	
  private:
	light_source * const _light;

	rgb_color _color;
	float _angle;
};

#endif
