/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#ifndef __WORLD_H
#define __WORLD_H

#include "context-part.h"
#include "game-machine.h"

class context;
class path_line;
class path_line_gen;
class path_frame;
class world;
class player;
class stuff_manager;
class light_source;
class stuff_gen;
class light_group;

class world : public context_part, public game_machine::state_callback
{
  public:
	enum 
	{
		flag_draw_tunnel = context_part::next_flag,
		flag_touch_control,
		next_flag,
	};

	world(context *context);

	void on_create() override;
	void on_window_resize(int width, int height) override;
	void on_touch_event(touch_event event) override;
	void on_game_state_changed(game_state state) override;

	void draw();
	glm::mat4 make_mvp_matrix(const glm::mat4 &model);
	void dirty_view();
	void dirty_projection();
	void reset_path_line();

	glm::mat4 get_projection_matrix();
	glm::mat4 get_view_matrix();
	glm::mat4 get_view_projection_matrix();

	light_group *get_light_group() const;
	path_frame *get_last_path_frame();
	path_frame *get_first_path_frame();

  private:
	void _reflect_player(glm::vec3 normal);

	light_group * const _light_group;

	glm::mat4 _projection_matrix;
	glm::mat4 _view_matrix;
	glm::mat4 _view_projection_matrix;

	float _last_x;
	float _last_y;
	int _dirty_flags;
};

#endif
