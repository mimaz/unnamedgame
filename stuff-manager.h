/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#ifndef __STUFF_MANAGER_H
#define __STUFF_MANAGER_H

#include "context-part.h"

class light_source;
class abstract_stuff;
class stuff;
class path_frame;

class stuff_manager : public context_part, public context_object::object_registry_listener
{
  public:
	stuff_manager(context *context);
	~stuff_manager();

	void preprocess() override;

	void on_object_registered(context_object *object) override;
	void on_object_unregistered(context_object *object) override;

	void delete_all_stuffs();
	void draw_stuffs();

	void on_push_path_frame_back(path_frame *fr);
	void on_pop_path_frame_front(path_frame *fr);

  private:
	void _on_create_segment(path_frame *from, path_frame *to);
	void _on_delete_segment(path_frame *from, path_frame *to);
	void _set_stuff_active(stuff *st, bool active);

	class stuff_comparator
	{
	  public:
		bool operator()(abstract_stuff *pst, abstract_stuff *qst) const;
	};

	std::set<abstract_stuff *, stuff_comparator> _unused_stuff_set;
	std::set<abstract_stuff *, stuff_comparator> _used_stuff_set;
	std::unordered_set<abstract_stuff *> _all_stuff_set;
};

#endif
