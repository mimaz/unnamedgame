/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#include <std.h>

#include "texture-effect.h"

LOCAL_BEGIN

enum
{
	a_coord,
	a_tex_coord,
};

const GLchar *blur_fade_vertex_code = R"glsl(
#version 310 es

in lowp vec2 a_coord;
in lowp vec2 a_tex_coord;

out lowp vec2 v_tex_coord;

uniform highp mat4 u_mvp;

void main()
{
	gl_Position = vec4(a_coord, 0.0, 1.0);

	v_tex_coord = a_tex_coord;
}
)glsl";

const GLchar *blur_fade_fragment_code = R"glsl(
#version 310 es

in lowp vec2 v_tex_coord;

layout(location = 0) out lowp vec4 frag_color;

uniform sampler2D u_texture;
uniform lowp float u_exposure;

const lowp int c_range = 4;

lowp vec2 get_coord(lowp int x, lowp int y)
{
	lowp float factor = (1.0 - u_exposure)
			* float(c_range)
			/ 64.0;

	x -= c_range / 2;
	y -= c_range / 2;
						
	return v_tex_coord + vec2(x, y) * factor;
}

void main()
{
	lowp vec2 coord = get_coord(0, 0);
	lowp vec4 color = texture2D(u_texture, coord);

	if (u_exposure < 0.999)
	{

		for (lowp int y = 0; y < c_range; y++)
			for (lowp int x = 0; x < c_range; x++)
			{
				coord = get_coord(x, y);

				color += texture2D(u_texture, coord);
			}

		color /= float(c_range * c_range + 1);

		lowp float alpha = u_exposure;

		alpha = 1.0 - alpha;
		alpha *= alpha * alpha * alpha;
		alpha = 1.0 - alpha;

		color.a *= alpha;
	}

	frag_color = color;
}
)glsl";

const GLfloat vertex_data[] = {
	1, 1, 1, 1,
	1, -1, 1, 0,
	-1, -1, 0, 0,

	1, 1, 1, 1,
	-1, -1, 0, 0,
	-1, 1, 0, 1,
};

GLchar logmsg[256];

GLuint compile_shader(const std::string &name, GLenum type, const GLchar *code)
{
	assert(type == GL_VERTEX_SHADER || type == GL_FRAGMENT_SHADER);

	GLuint sh = glCreateShader(type);
	glShaderSource(sh, 1, &code, nullptr);
	glCompileShader(sh);

	GLint ok;
	glGetShaderiv(sh, GL_COMPILE_STATUS, &ok);

	if (! ok)
	{
		std::string type_name = type == GL_VERTEX_SHADER 
			? "vertex" 
			: type == GL_FRAGMENT_SHADER 
			? "fragment"
			: "unknown";

		glGetShaderInfoLog(sh, 256, nullptr, logmsg);
		std::cerr << "compilling " << type_name << " shader for " 
			<< name << " failed: " << logmsg << std::endl;
		abort();
	}

	return sh;
}

GLuint link_program(const std::string &name, 
					std::initializer_list<std::pair<const GLchar *, GLuint>> attrs,
					const GLchar *vcode, 
					const GLchar *fcode)
{
	GLuint vsh = compile_shader(name, GL_VERTEX_SHADER, vcode);
	GLuint fsh = compile_shader(name, GL_FRAGMENT_SHADER, fcode);

	GLuint program = glCreateProgram();
	glAttachShader(program, vsh);
	glAttachShader(program, fsh);
	glLinkProgram(program);

	GLint ok;
	glGetProgramiv(program, GL_LINK_STATUS, &ok);

	if (! ok)
	{
		glGetProgramInfoLog(program, 256, nullptr, logmsg);
		std::cerr << "linking " << name << " program failed: "
			<< logmsg << std::endl;
		abort();
	}

	glDeleteShader(vsh);
	glDeleteShader(fsh);

	return program;
}

class blur_fade : public context_part
{
  public:
	blur_fade(context *context)
		: context_part(context)
	{}

	void on_create() override
	{
		context_part::on_create();

		program = link_program("blur_fade", 
							   { { "a_coord", a_coord }, 
							   	 { "a_tex_coord", a_tex_coord } },
							   blur_fade_vertex_code, 
							   blur_fade_fragment_code);

		u_mvp = glGetUniformLocation(program, "u_mvp");
		u_exposure = glGetUniformLocation(program, "u_exposure");
	}

	void draw(float exposure, const glm::mat4 &matrix)
	{
		glUseProgram(program);

		glEnableVertexAttribArray(a_coord);
		glEnableVertexAttribArray(a_tex_coord);

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glVertexAttribPointer(a_coord, 2, GL_FLOAT, GL_FALSE,
							  sizeof(GLfloat) * 4,
							  vertex_data);
		glVertexAttribPointer(a_tex_coord, 2, GL_FLOAT, GL_FALSE,
							  sizeof(GLfloat) * 4,
							  vertex_data + 2);

		glUniformMatrix4fv(u_mvp, 1, GL_FALSE, glm::value_ptr(matrix));
		glUniform1f(u_exposure, exposure);

		glDrawArrays(GL_TRIANGLES, 0, 6);

		glDisableVertexAttribArray(a_coord);
		glDisableVertexAttribArray(a_tex_coord);
	}

	GLuint program;
	GLuint u_mvp;
	GLuint u_exposure;
};

LOCAL_END

texture_effect::texture_effect(context *context)
	: context_part(context)
{}

texture_effect::~texture_effect()
{}

void texture_effect::draw_with_blur_effect(GLuint texture, 
										   float exposure)
{
	glm::mat4 matrix = glm::ortho(-0.5, 0.5, -0.5, 0.5);

	draw_with_blur_effect(texture, matrix, exposure);
}

void texture_effect::draw_with_blur_effect(GLuint texture, 
										   const glm::mat4 &matrix, 
										   float exposure)
{
	glActiveTexture(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texture);

	get_part<blur_fade>()->draw(exposure, matrix);
}
