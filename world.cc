/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#include <std.h>

#include "world.h"
#include "context.h"
#include "tunnel.h"
#include "player.h"
#include "path-gen.h"
#include "path-line.h"
#include "light-source.h"
#include "dynamic-lighting.h"
#include "light-box.h"
#include "stuff-manager.h"
#include "light-group.h"
#include "stuff-gen.h"

LOCAL_BEGIN

float NEAR_PLANE = 0.01;

enum
{
	DIRTY_VIEW_MATRIX = 0x1,
	DIRTY_PROJECTION_MATRIX = 0x2,
	DIRTY_VIEW_PROJECTION_MATRIX = 0x4,
	DIRTY_REFLECTION = 0x8,
	DIRTY_ALL = DIRTY_VIEW_MATRIX 
		| DIRTY_PROJECTION_MATRIX
		| DIRTY_VIEW_PROJECTION_MATRIX
};

LOCAL_END

/*
 * world class
 */

world::world(context *context)
	: context_part(context)
	, _light_group(create_object<light_group>(this))
	, _dirty_flags(DIRTY_ALL)
{}

void world::on_create()
{
	context_part::on_create();

	get_part<tunnel>()->get_lighting()->set_light_group(get_light_group());
	get_light_group()->register_light(get_part<player>()->get_light());

	reset_path_line();
}

void world::on_window_resize(int width, int height)
{
	context_part::on_window_resize(width, height);

	_dirty_flags |= DIRTY_PROJECTION_MATRIX 
		| DIRTY_VIEW_PROJECTION_MATRIX;
}

void world::on_touch_event(touch_event event)
{
	context_part::on_touch_event(event);

	float movex = event.x() - _last_x;
	float movey = event.y() - _last_y;

	switch (event)
	{
	  case touch_event::MOVE:
		if (is_flag_set(flag_touch_control))
		{
			get_part<world>()->get_part<player>()->rotate_view(movex, { 0, 1, 0 });
			get_part<world>()->get_part<player>()->rotate_view(movey, { -1, 0, 0 });
		}
		break;

	  default:
		break;
	}

	_last_x = event.x();
	_last_y = event.y();
}

void world::on_game_state_changed(game_state state)
{
	switch (state)
	{
	  case game_state::menu:
		break;

	  default:
		break;
	}
}

void world::draw() 
{
	glEnable(GL_DEPTH_TEST);

	get_part<tunnel>()->draw();
	get_part<player>()->draw();
	get_part<stuff_manager>()->draw_stuffs();
}

glm::mat4 world::make_mvp_matrix(const glm::mat4 &model)
{ return get_view_projection_matrix() * model; }

void world::dirty_view()
{
	_dirty_flags |= DIRTY_VIEW_MATRIX 
		| DIRTY_VIEW_PROJECTION_MATRIX;
}

void world::dirty_projection()
{
	_dirty_flags |= DIRTY_PROJECTION_MATRIX 
		| DIRTY_VIEW_PROJECTION_MATRIX; 
}

void world::reset_path_line()
{
	get_part<path_line>()->clear_path_line();

	// build the initial tunnel path_frames
	for (int i = 0; i < 10; i++)
		get_part<path_line>()->emplace_path_frame_back(glm::mat4(1));
}

glm::mat4 world::get_projection_matrix()
{ 
	if (_dirty_flags & DIRTY_PROJECTION_MATRIX)
	{
		float aspect = get_context()->get_wh_aspect_ratio();

		if (aspect != aspect
				|| aspect > 100)
		{
			aspect = 1;
		}

		_projection_matrix = glm::perspective(
				static_cast<float>(M_PI) / 4,
				aspect,
				NEAR_PLANE, 
				get_part<player>()->get_view_range());
	}

	return _projection_matrix; 
}

glm::mat4 world::get_view_matrix()
{
	if (_dirty_flags & DIRTY_VIEW_MATRIX)
	{
		_dirty_flags &= ~DIRTY_VIEW_MATRIX;

		_view_matrix = glm::mat4(1);
		_view_matrix = glm::rotate(glm::mat4(1), PI, glm::vec3(0, 0, 1));
		_view_matrix *= glm::rotate(glm::mat4(1), PI, glm::vec3(0, 1, 0));
		_view_matrix *= get_part<player>()->get_matrix();
	}

	return _view_matrix;
}

glm::mat4 world::get_view_projection_matrix() 
{
	if (_dirty_flags & DIRTY_VIEW_PROJECTION_MATRIX)
	{
		_dirty_flags &= ~DIRTY_VIEW_PROJECTION_MATRIX;

		_view_projection_matrix = get_projection_matrix() 
								* get_view_matrix();
	}

	return _view_projection_matrix;
}

light_group *world::get_light_group() const
{ return _light_group; }

path_frame *world::get_last_path_frame()
{ return get_part<path_line>()->get_last_path_frame(); }

path_frame *world::get_first_path_frame()
{ return get_part<path_line>()->get_first_path_frame(); }

void world::_reflect_player(glm::vec3 normal)
{
	glm::vec3 player_look = get_part<player>()->get_matrix() * glm::vec4(0, 0, 1, 0);

	glm::vec3 axis = glm::cross(normal, player_look);
	axis = glm::normalize(axis);

	float cosine = glm::dot(normal, player_look);
	float angle = (M_PI / 2 - acosf(cosine)) * 2;

	get_part<player>()->rotate_view(angle, axis);
}
