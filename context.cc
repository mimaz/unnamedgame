/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#include <std.h>

#include "context.h"
#include "context-object.h"
#include "context-part.h"
#include "game-machine.h"
#include "world.h"
#include "stuff-gen.h"
#include "path-gen.h"

/*
 * context class
 */

context *context::create_context()
{
	context *ctx = new context;
	ctx->on_create();
	return ctx;
}

void context::delete_context(context *ctx)
{
	ctx->on_destroy();
	delete ctx;
}

void context::on_create()
{
	context_object::on_create();

	glClearColor(0.0, 0.0, 0.0, 1.0);
	glFrontFace(GL_CW);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	get_part<world>();
	get_part<stuff_gen>();
	get_part<path_line_gen>();
	get_part<game_machine>()->switch_state(game_state::menu);
}

void context::draw()
{
	preprocess();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glViewport(0, 0, get_window_width(), get_window_height());

	get_part<world>()->draw();
}

void context::on_resize_path_frame(int width, int height)
{
	if (width != _path_frame_width || height != _path_frame_height)
	{
		glViewport(0, 0, width, height);

		_path_frame_width = width;
		_path_frame_height = height;

		for (auto pp : _part_map)
			pp.second->on_window_resize(width, height);
	}
}

void context::on_touch_event(touch_event event)
{
	for (auto pp : _part_map)
		pp.second->on_touch_event(event);
}

void context::register_object(context_object *object)
{ 
	_object_sett.insert(object); 

	for (object_registry_listener *orl : _registry_listener_set)
		orl->on_object_registered(object);



	{
		context_part *part = dynamic_cast<context_part *>(object);

		if (part)
		{
			assert(_part_map[part->get_hash()] == nullptr);

			_part_map[part->get_hash()] = part;
		}
	}



	{
		object_registry_listener *regl = dynamic_cast<object_registry_listener *>(object);

		if (regl)
		{
			for (context_object *object : _object_sett)
				regl->on_object_registered(object);

			_registry_listener_set.insert(regl);
		}
	}
}

void context::unregister_object(context_object *object)
{ 
	_object_sett.erase(object); 

	for (object_registry_listener *orl : _registry_listener_set)
		orl->on_object_unregistered(object);



	{
		context_part *part = dynamic_cast<context_part *>(object);

		if (part)
		{
			assert(_part_map[part->get_hash()] != nullptr);

			_part_map[part->get_hash()] = nullptr;
		}
	}



	{
		object_registry_listener *regl = dynamic_cast<object_registry_listener *>(object);

		if (regl)
		{
			assert(_registry_listener_set.find(regl) != _registry_listener_set.end());

			_registry_listener_set.erase(regl);
		}
	}
}

context_part *context::get_part_ptr(const std::type_info &tinfo)
{ return _part_map[tinfo.hash_code()]; }

void context::delete_object(context_object *object)
{
	object->on_destroy();
	set_flag(flag_deletable_object);
	delete object;
}

int context::get_window_width() const
{ return _path_frame_width; }

int context::get_window_height() const
{ return _path_frame_height; }

float context::get_wh_aspect_ratio() const
{
	return static_cast<float>(get_window_width()) / get_window_height();
}

float context::get_hw_aspect_ratio() const
{
	return static_cast<float>(get_window_height()) / get_window_width();
}

context::context()
	: context_object(this)
{}

context::~context()
{}
