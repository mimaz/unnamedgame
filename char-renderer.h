/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#ifndef __CHAR_RENDERER_H
#define __CHAR_RENDERER_H

#include "context-part.h"

class char_renderer : public context_part
{
  public:
	class character_data;

	char_renderer(context *context);
	~char_renderer();

	void renderer_character(int code, 
							const glm::mat4 &matrix);

	void set_color(glm::vec4 color);
	void set_line_width(float width);

	glm::vec4 get_color() const;
	float get_line_width() const;

  private:
	void _gen_vbo();

	glm::vec4 _color;
	float _line_width;

	GLuint _program;
	GLuint _vbo;
	GLuint _a_coord;
	GLuint _u_matrix;
	GLuint _u_line_width;
	GLuint _u_color;
	std::unordered_map<int, character_data *> _data_map;
};

#endif
