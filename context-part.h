/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#ifndef __CONTEXT_PART_H
#define __CONTEXT_PART_H

#include "enums.h"
#include "context-object.h"

class context_part : public context_object
{
  public:
	context_part(context *ctx);

	virtual void on_window_resize(int width, int height);
	virtual void on_touch_event(touch_event event);
};

#endif
