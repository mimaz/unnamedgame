/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#include <std.h>

#include "game-machine.h"

game_machine::game_machine(context *ctx)
	: context_part(ctx)
{}

void game_machine::on_object_registered(context_object *object)
{
	state_callback *cb = dynamic_cast<state_callback *>(object);

	if (cb)
	{
		//std::cout << "register callback: " << typeid(*object).name() << std::endl;
		_callback_set.insert(cb);
	}
}

void game_machine::on_object_unregistered(context_object *object)
{
	state_callback *cb = dynamic_cast<state_callback *>(object);

	if (cb)
	{
		//std::cout << "unregister callback: " << typeid(*object).name() << std::endl;
		_callback_set.erase(cb);
	}
}

void game_machine::switch_state(game_state state)
{
	switch (state)
	{
	  case game_state::menu:
		_switch_menu();
		break;

	  case game_state::play:
		_switch_play();
		break;

	  default:
		std::cerr << "invalid state! " << state.to_string() << std::endl;
	}
}

game_state game_machine::get_state() const
{ return _state; }

void game_machine::_switch_menu()
{
	assert(get_state() == game_state::none);

	_set_state(game_state::menu);

	std::cout << "menu!" << std::endl;
}

void game_machine::_switch_play()
{
	assert(get_state() == game_state::menu);

	_set_state(game_state::play);
}

void game_machine::_set_state(game_state state)
{
	_state = state;

	for (state_callback *cb : _callback_set)
		cb->on_game_state_changed(state);
}
