/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#ifndef __DYNAMIC_LIGHTING_H
#define __DYNAMIC_LIGHTING_H

#include "context-object.h"

/*
 * dynamic_lighting fragment shader implements glsl function
 *
 * vec3 calculate_diffuse_light(vec3 coord, vec3 normal, 
 * 								vec3 camera, float view_range)
 *
 * coord: position of fragment
 * normal: normal of surface with length equal 1.0
 * camera: position of camera
 * camera_range: radius of view range
 *
 * and returns combined vector of diffuse light color
 */

class light_source;
class light_group;

class dynamic_lighting : public context_object
{
  public:
	class shader;
	class builder;

	static builder build();

	dynamic_lighting(context_object *parent);
	~dynamic_lighting();

	void set_light_group(const light_group *group);

	shader *use_program();
	shader *use_program(const light_group *group);
	shader *use_program(glm::vec3 position);
	shader *use_program(glm::vec3 position, const light_group *group);

	const light_group *get_light_group() const;

  private:
	shader *_get_shader(int);
	shader *_use_program(int, const light_source **);

	const light_group *_light_group;

	GLuint _vshader;
	shader *(*_shader_allocator)(void);
	std::vector<const GLchar *> _fragment_sources;

	std::array<shader *, 50> _shaders;

	friend class builder;
};

class dynamic_lighting::shader
{
  public:
	virtual ~shader();

	void build(GLuint fcnt, 
			   const GLchar * const * fsrc, 
			   GLuint vsh, 
			   int count);

	virtual void resolve();

	GLuint program;
	GLuint u_light_positions;
	GLuint u_light_colors;
	GLuint u_light_ranges;
};

class dynamic_lighting::builder
{
  public:
	builder();

	builder &vertex_source(GLuint vcnt,
						   const GLchar * const * vsrc);
	builder &fragment_source(GLuint fcnt, 
							 const GLchar * const * fsrc);
	builder &shader_allocator(shader *(*allocator)(void));
	builder &light_source_group(const light_group *group);
	builder &parent(context_object *parent);

	dynamic_lighting *done();

  private:
	GLuint _vcnt;
	const GLchar * const * _vsrc;
	GLuint _fcnt;
	const GLchar * const * _fsrc;
	shader *(*_allocator)(void);
	const light_group *_light_group;
	context_object *_parent;
};

#endif
