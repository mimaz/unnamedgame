/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#ifndef __PATH_GEN_H
#define __PATH_GEN_H

#include "context-part.h"
#include "game-machine.h"

class path_line;
class path_frame;

class path_line_gen : public context_part, public game_machine::state_callback
{
  public:
	class shape
	{
	  public:
		int count;
		float angle;
		glm::vec3 axis;
	};

	path_line_gen(context *context);

	void on_game_state_changed(game_state state) override;

	void preprocess() override;

	void reset_shape();

  private:

	shape _shape;
	shape (*_shape_generator)(float);
};

#endif
