/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#include <std.h>

#include "stuff-manager.h"
#include "stuff.h"
#include "light-box.h"
#include "rect-obstacle.h"
#include "player.h"
#include "path-line.h"
#include "path-frame.h"
#include "stuff-gen.h"

bool stuff_manager::stuff_comparator::operator()(abstract_stuff *pst, 
												 abstract_stuff *qst) const
{
	return pst->v_get_location() != qst->v_get_location() 
		? pst->v_get_location() < qst->v_get_location()
		: pst < qst;
}

class stuff_comp : public abstract_stuff
{
  public:
	stuff_comp(float loc)
		: _location(loc)
	{}

	float v_get_location() const override 
	{ return _location; }

  private:
	float _location;
};

/*
 * stuff_manager class
 */

stuff_manager::stuff_manager(context *context)
	: context_part(context)
{}

stuff_manager::~stuff_manager()
{}

void stuff_manager::preprocess()
{
	context_part::preprocess();

	glm::vec3 player_pos = get_part<player>()->get_matrix() * glm::vec4(0, 0, 0, 1);

	for (abstract_stuff *as : _used_stuff_set)
	{
		stuff *st = static_cast<stuff *>(as);

		glm::vec3 correction;
		glm::vec3 normal;
		bool test = st->test_for_collision(player_pos, &correction, &normal);

		if (test)
		{
			glm::mat4 matrix = glm::translate(glm::mat4(1), correction) * get_part<player>()->get_matrix();

			get_part<player>()->set_matrix(matrix);
			get_part<player>()->reflect(normal);
		}
	}
}

void stuff_manager::on_object_registered(context_object *object)
{
	stuff *st = dynamic_cast<stuff *>(object);

	if (st)
	{
		path_frame *fr = get_part<path_line>()->find_path_frame_by_location(st->get_location());

		_all_stuff_set.insert(st);

		if (fr)
		{
			st->set_path_frame(fr);
			_set_stuff_active(st, true);
		}
		else
		{
			_set_stuff_active(st, false);
		}
	}
}

void stuff_manager::on_object_unregistered(context_object *object)
{
	stuff *st = dynamic_cast<stuff *>(object);

	if (st)
	{
		_unused_stuff_set.erase(st);
		_used_stuff_set.erase(st);
		_all_stuff_set.erase(st);
	}
}

void stuff_manager::delete_all_stuffs()
{
	std::vector<abstract_stuff *> removable(
			_all_stuff_set.begin(), _all_stuff_set.end());

	assert(_all_stuff_set.size() == _unused_stuff_set.size() + _used_stuff_set.size());

	for (abstract_stuff *st : removable)
		delete_object(static_cast<stuff *>(st));

	assert(_unused_stuff_set.empty());
	assert(_used_stuff_set.empty());
	assert(_all_stuff_set.empty());
}

void stuff_manager::draw_stuffs()
{
	for (abstract_stuff *as : _used_stuff_set)
		static_cast<stuff *>(as)->draw();
}

void stuff_manager::on_push_path_frame_back(path_frame *fr)
{
	if (fr->get_prev())
		_on_create_segment(fr->get_prev(), fr);
}

void stuff_manager::on_pop_path_frame_front(path_frame *fr)
{
	if (fr->get_next())
		_on_delete_segment(fr, fr->get_next());
}

void stuff_manager::_on_create_segment(path_frame *from, path_frame *to)
{
	stuff_comp bg_st = from->get_location();
	stuff_comp nd_st = to->get_location();

	auto it = _unused_stuff_set.lower_bound(&bg_st);
	auto nd = _unused_stuff_set.lower_bound(&nd_st);

	while (it != nd)
	{
		stuff *st = static_cast<stuff *>(*it++);

		_set_stuff_active(st, true);

		path_frame *closer = std::abs(from->get_location() - st->get_location())
			< std::abs(to->get_location() - st->get_location())
			? from 
			: to;

		st->set_path_frame(closer);
	}
}

void stuff_manager::_on_delete_segment(path_frame *from, path_frame *to)
{
	stuff_comp bg_st = from->get_location();
	stuff_comp nd_st = to->get_location();

	auto it = _used_stuff_set.lower_bound(&bg_st);
	auto nd = _used_stuff_set.lower_bound(&nd_st);

	while (it != nd)
	{
		stuff *st = static_cast<stuff *>(*it++);

		_set_stuff_active(st, false);

		st->set_path_frame(nullptr);
	}
}

void stuff_manager::_set_stuff_active(stuff *st, bool active)
{
	st->set_active(active);

	if (active)
	{
		_used_stuff_set.insert(st);
		_unused_stuff_set.erase(st);
	}
	else
	{
		_used_stuff_set.erase(st);
		_unused_stuff_set.insert(st);
	}

	if (! active 
			&& st->is_flag_set(stuff::flag_delete_on_inactive))
	{
		delete_object(st);
	}
}
