/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#ifndef __SURFACE_H
#define __SURFACE_H

#include "enums.h"
#include "context-part.h"

class surface_item;

class surface : public context_part
{
  public:
	surface(context *context);
	~surface();

	void on_draw();
	void on_create() override;
	void on_window_resize(int width, int height) override;
	void on_touch_event(touch_event event);
	void draw_texture(GLuint texture, int x, int y, int width, int height);
	glm::mat4 make_mvp(const glm::mat4 &model);

	void register_item(surface_item *item);
	void unregister_item(surface_item *item);

	context *get_context() const;
	int get_width() const;
	int get_height() const;
	glm::mat4 get_matrix();

  private:
	std::unordered_set<surface_item *> _item_set;

	context * const _context;
	
	glm::mat4 _matrix;
	int _width;
	int _height;
	int _dirty_flags;
	float _fade_progress;
	bool _fade_flag;

	GLuint _program;
	GLuint _u_mvp;
};

#endif
