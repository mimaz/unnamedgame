/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#include <std.h>

#include "gradient-gyroscope.h"

gradient_gyroscope::gradient_gyroscope()
	: _momentum(0)
{}

void gradient_gyroscope::tick()
{
	const float sample = 0.01;

	glm::vec3 vector = get_vector();
	glm::vec3 axis = get_axis();
	glm::vec3 target = get_target();

	float dot = glm::dot(target, vector);

	glm::mat4 matrix = glm::rotate(glm::mat4(1), sample, axis);
	glm::vec3 new_vector = matrix * glm::vec4(vector, 0);

	float new_dot = glm::dot(target, new_vector);

	float gradient = (new_dot - dot) 
		/ sample
		* 0.05;

	if (gradient == gradient)
	{
		_momentum += gradient;
		_momentum *= 0.8;

		rotate(_momentum);
	}
}
