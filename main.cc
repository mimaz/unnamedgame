/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#include <std.h>

#include "world.h"
#include "player.h"
#include "context.h"

static GLFWwindow *window;
static context *ctx;
static double xpos, ypos;
static int width, height;
static bool pressed;

static void key_callback(GLFWwindow *window, 
						 int key, int scancode, 
						 int action, int mods)
{
	bool pressed = action != GLFW_RELEASE;

	switch (key)
	{
	  case GLFW_KEY_E:
		if (! pressed)
		{
			/*
			float speed = ctx->get_part<world>()->get_part<player>()->get_target_speed() > 1 ?
				0 : 50;
			ctx->get_part<world>()->get_part<player>()->set_target_speed(speed);
			*/
		}
		break;

	  case GLFW_KEY_ESCAPE:
		if (! pressed)
			glfwSetWindowShouldClose(window, GLFW_TRUE);
		break;
	}
}

static void handle_touch_event(touch_event event)
{
	int width, height;
	glfwGetWindowSize(window, &width, &height);

	event.x() = xpos / width - 0.5;
	event.y() = ypos / height - 0.5;

	ctx->on_touch_event(event);
}

static void mouse_position_callback(GLFWwindow *window,
									double xpos,
									double ypos)
{
	::xpos = xpos;
	::ypos = ypos;

	if (pressed)
		handle_touch_event(touch_event::MOVE);
}

static void mouse_click_callback(GLFWwindow *window, 
								 int button, int action, 
								 int mods)
{
	touch_event event;

	switch (action)
	{
	  case GLFW_PRESS:
		event = touch_event::PRESS;
		pressed = true;
		break;

	  case GLFW_RELEASE:
		event = touch_event::RELEASE;
		pressed = false;
		break;
	
	  default:
		assert(false);
	};

	handle_touch_event(event);
}

static void path_frame_resize_callback(GLFWwindow *window,
								  int width, int height)
{
	::width = width;
	::height = height;

	ctx->on_resize_path_frame(width, height);
}

int main()
{
	srand(clock());

	glfwInit();

	glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_ES_API);
	glfwWindowHint(GLFW_CONTEXT_CREATION_API, GLFW_NATIVE_CONTEXT_API);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);

	window = glfwCreateWindow(960, 680, "My Title", 
							  // use fullscreen ?
							  nullptr, 
							  //glfwGetPrimaryMonitor(),
							  nullptr);
	assert(window);

	glfwMakeContextCurrent(window);


	glfwSetKeyCallback(window, key_callback);
	glfwSetCursorPosCallback(window, mouse_position_callback);
	glfwSetMouseButtonCallback(window, mouse_click_callback);
	glfwSetFramebufferSizeCallback(window, path_frame_resize_callback);

	ctx = context::create_context();

	const double path_frame_delay = 1.0 / 60.0;

	double next_time = glfwGetTime() + path_frame_delay;

	while (! glfwWindowShouldClose(window))
	{
		double time = glfwGetTime();
		bool yield = true;

		if (time > next_time)
		{
			next_time += path_frame_delay;

			ctx->draw();

			glfwSwapBuffers(window);
			glfwPollEvents();

			yield = false;
		}

		if (yield)
			std::this_thread::yield();
	}

	context::delete_context(ctx);

	std::cout << "created objects: " << context_object::created
		<< std::endl << "destroyed objects: " << context_object::destroyed
		<< std::endl;

	glfwDestroyWindow(window);
	glfwTerminate();

	return 0;
}


