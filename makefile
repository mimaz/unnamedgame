##
 # Created by Mieszko Mazurek <mimaz@gmx.com>
 ##

project = game
sources = main.cc \
		  context.cc \
		  world.cc \
		  tunnel.cc \
		  path-line.cc \
		  path-gen.cc \
		  char-renderer.cc \
		  surface.cc \
		  main-menu.cc \
		  light-source.cc \
		  player.cc \
		  dynamic-lighting.cc \
		  surface-item.cc \
		  light-box.cc \
		  stuff-manager.cc \
		  stuff.cc \
		  path-frame.cc \
		  enums.cc \
		  rect-obstacle.cc \
		  stuff-gen.cc \
		  light-group.cc \
		  text-renderer.cc \
		  buffered-text.cc \
		  menu-button.cc \
		  gradient-gyroscope.cc \
		  texture-effect.cc \
		  context-object.cc \
		  context-part.cc \
		  game-machine.cc

build_dir = /tmp/game-obj


################################

tmpincdir = ${build_dir}/pch
stdpch = ${tmpincdir}/std.h.gch
optimization = -Og -g
#optimization = -O1 -fno-lto
#optimization = -O2 -fno-lto
#optimization = -O3 -flto -fno-fat-lto-objects
cxxflags = ${optimization} -Wall -std=c++11 -DGLM_FORCE_RADIANS -MMD -I${tmpincdir}
ldflags = ${optimization} -lglfw -lGL

target = ${project}.elf



objects = ${sources:%.cc=${build_dir}/%.o}
depfiles = ${objects:%.o=%.d}



all: ${target}

std: ${stdpch}

run: ${target}
	./$<

clean: 
	rm -rf ${build_dir} ${target} ${stdpch}

${target}: ${objects}
	g++ ${objects} -o $@ ${ldflags}

${build_dir}/%.o: %.cc ${stdpch}
	@mkdir -p ${dir $@}
	g++ ${cxxflags} -c $< -o $@

${stdpch}: std.h
	@mkdir -p ${dir $@}
	g++ ${cxxflags} -x c++-header -c $< -o $@

 
-include ${depfiles}
