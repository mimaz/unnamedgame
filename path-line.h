/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#ifndef __PATH_LINE_H
#define __PATH_LINE_H

#include "context-part.h"

class path_frame;

class path_line : public context_part
{
  public:
	path_line(context *context);

	void clear_path_line();

	void push_path_frame_back(path_frame *fr);
	path_frame *pop_path_frame_front();

	void emplace_path_frame_back(const glm::mat4 &matrix);
	void delete_path_frame_front();

	path_frame *find_path_frame_by_location(float location) const;

	path_frame *get_first_path_frame() const;
	path_frame *get_last_path_frame() const;
	float get_start_location() const;
	float get_end_location() const;

  private:
	void _set_initial_path_frame(path_frame *fr);
	void _cleanup();

	path_frame *_first_path_frame;
	path_frame *_last_path_frame;
};

#endif
