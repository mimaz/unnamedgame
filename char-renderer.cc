/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#include <std.h>

#include "char-renderer.h"

static const GLchar * const vsrc = R"(
#version 310 es
in lowp vec2 a_coord;

uniform highp mat4 u_matrix;
uniform highp float u_line_width;

void main()
{
	lowp vec4 coord = vec4(a_coord, 0.0, 1.0);

	gl_Position = u_matrix * coord;
	gl_PointSize = u_line_width;
}
)";

static const GLchar * const fsrc = R"(
#version 310 es
uniform lowp vec4 u_color;

layout(location = 0) out lowp vec4 frag_color;

void main()
{
	frag_color = u_color;
}
)";

constexpr int character_width = 5;
constexpr int character_height = 7;

class character
{
  public:
	character(const char *map, const char *order, int code)
		: map(map), order(order), code(code)
	{}

    const char * const map;
	const char * const order;
	const int code;
};

static const character characters[] = {
	{	"1   2"
		"     "
		"     "
		"  3  "
		"     "
		"     "
		"4   5", "#ls12541 #l1524", 0 },
	{	"     "
		"     "
		"     "
		"     "
		"     "
		" 12  "
		" 43  ", "#ls12341", '.' },
	{	"     "
		"     "
		"     "
		"     "
		" 1   "
		"     "
		"  3  ", "#l13", ',' },
	{	"     "
		"     "
		"     "
		"     "
		"     "
		"     "
		"     ", "", ' ' },
	{	"  1  "
		"     "
		"     "
		"     "
		"  2  "
		"  3  "
		"4   5", "#ls415 #l23", 'A' },
	{	"12 3 "
		"    4"
		"     "
		"5   6"
		"     "
		"    7"
		"89 A ", "#ls2347A9 #l8156", 'B' },
	{	" 1 2 "
		"3   4"
		"     "
		"     "
		"     "
		"5   6"
		" 7 8 ", "#ls42135786", 'C' },
	{	"1  2 "
		"    3"
		"     "
		"     "
		"     "
		"4   5"
		"6  7 ", "#ls4123576", 'D' },
	{	"1   2"
		"     "
		"     "
		"3  4 "
		"     "
		"     "
		"5   6", "#ls6512 #l34", 'E' },
	{	"1   2"
		"     "
		"     "
		"3  4 "
		"     "
		"     "
		"5    ", "#ls512 #l34", 'F' },
	{	" 1  2"
		"3    "
		"     "
		"  4 5"
		"     "
		"6    "
		" 7  8", "#ls45876312", 'G' },
	{	" 1  2"
		"3    "
		"     "
		"4   5"
		"     "
		"     "
		"6   7", "#ls136 #l2745", 'H' },
	{	" 123 "
		"     "
		"     "
		"     "
		"     "
		"     "
		" 456 ", "#l134625", 'I' },
	{	"    1"
		"     "
		"     "
		"     "
		"2    "
		"     "
		"3   4", "#ls2341", 'J' },
	{	"1   2"
		"     "
		" 3   "
		"4    "
		"     "
		"     "
		"5   6", "#ls245 #l1436", 'K' },
	{	"1    "
		"     "
		"     "
		"     "
		"     "
		"2    "
		" 3  4", "#ls1234", 'L' },
	{	"1   2"
		"     "
		"     "
		"     "
		"  3  "
		"     "
		"4   5", "#ls41325", 'M' },
	{	"1   2"
		"     "
		"     "
		"     "
		"     "
		"     "
		"3   4", "#ls3142", 'N' },
	{	" 1 2 "
		"3   4"
		"     "
		"     "
		"     "
		"5   6"
		" 7 8 ", "#ls124687531", 'O' },
	{	"1  2 "
		"    3"
		"    4"
		"5  6 "
		"     "
		"     "
		"7    ", "#ls75123465", 'P' },
	{	"1  2 "
		"    3"
		"    4"
		"5  6 "
		"     "
		"     "
		"7   8", "#ls751234658", 'R' },
	{	" 1 2 "
		"3   4"
		"     "
		"     "
		"     "
		"5   6"
		" 7 8 ", "#ls57863124", 'S' },
	{	" 12 3"
		"4    "
		"     "
		"     "
		"     "
		"     "
		"  5  ", "#ls413 #l25", 'T' },
	{	"1   2"
		"     "
		"     "
		"     "
		"     "
		"3   4"
		" 5 6 ", "#ls135642", 'U' },
	{	"1   2"
		"     "
		"     "
		"     "
		"     "
		"     "
		"  3  ", "#ls132", 'V' },
	{	"1   2"
		"     "
		"     "
		"     "
		"  3  "
		"     "
		" 4 5 ", "#ls14352", 'W' },
	{	"1   2"
		"     "
		"     "
		"     "
		"     "
		"     "
		"3   4", "#l1423", 'X' },
	{	"1   2"
		"     "
		"  3  "
		"     "
		"     "
		"     "
		"  4  ", "#ls134 #l32", 'Y' },
	{	"1   2"
		"     "
		"     "
		"     "
		"     "
		"     "
		"3   4", "#ls1234", 'Z' },
};

static const character *character_by_code(int code)
{
	for (const character &chr : characters)
		if (chr.code == code)
			return &chr;

	return characters;
}

class draw_call
{
  public:
	draw_call(GLenum mode, std::vector<GLubyte> indices)
		: mode(mode), indices(indices)
	{}

	const GLenum mode;
	const std::vector<GLubyte> indices;
};

static bool is_mapped_character(int code)
{
	return (code >= '0' && code <= '9') ||
		   (code >= 'a' && code <= 'z') ||
		   (code >= 'A' && code <= 'Z');
}

static int detect_mode(const char *str, GLenum &mode)
{
	if (str[0] == 'l')
	{
		if (str[1] == 's')
		{
			mode = GL_LINE_STRIP;
			return 2;
		}
		else
		{
			mode = GL_LINES;
			return 1;
		}
	}

	return 0;
}

static void compile_character(int code, std::vector<draw_call> &draw_calls)
{
	const int index_limit = character_width * character_height;

	const character *chr = character_by_code(code);

	std::unordered_map<int, int> symbol_map;



	const char *map = chr->map;
	int index = 0;

	while (*map)
	{
		assert(index < index_limit);

		char c = *map++;

		if (symbol_map.find(c) != symbol_map.end())
		{
			std::cerr << "symbol " << c 
				<< " was used more than once!" << std::endl;
			abort();
		}

		if (is_mapped_character(c))
			symbol_map[c] = index;

		index++;
	}

	if (index < index_limit)
	{
		std::cerr << "character map was too short!" << std::endl;
		abort();
	}


	const GLenum NONE = 99999999;

	std::vector<GLubyte> indices;
	GLenum mode = NONE;

	auto commit_call = [&]() -> void {
		if (mode == NONE)
		{
			std::cerr << "drawing mode was not set!" << std::endl;
			abort();
		}

		draw_calls.emplace_back(mode, indices);

		mode = NONE;
	};


	const char *order = chr->order;

	while (*order)
	{
		char c = *order++;

		if (c == '#')
		{
			int ln = detect_mode(order, mode);

			if (ln < 1)
			{
				std::cout << "unknown drawing mode: " 
					<< order << std::endl;
				abort();
			}

			order += ln;
			indices.clear();
		}
		else if (is_mapped_character(c))
		{
			if (mode == NONE)
			{
				std::cerr << "drawing mode was not set!" << std::endl;
				abort();
			}

			if (symbol_map.find(c) == symbol_map.end())
			{
				std::cerr << "character " << c 
					<< " was not mapped!" << std::endl;
				abort();
			}

			indices.push_back(symbol_map[c]);
		}
		else
		{
			commit_call();
		}
	}

	if (mode != NONE)
		commit_call();
}

class char_renderer::character_data
{
  public:
	character_data(int code)
	{ compile_character(code, _draw_calls); }

	void draw()
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		for (draw_call &dc : _draw_calls)
		{
			glDrawElements(dc.mode, dc.indices.size(), 
						   GL_UNSIGNED_BYTE, dc.indices.data());
		}
	}

  private:
	std::vector<draw_call> _draw_calls;
};

char_renderer::char_renderer(context *context)
	: context_part(context)
	, _color(1, 1, 1, 1)
	, _line_width(5)
{
	static GLchar logmsg[256];

	GLint ok;

	GLuint vsh = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vsh, 1, &vsrc, nullptr);
	glCompileShader(vsh);
	glGetShaderiv(vsh, GL_COMPILE_STATUS, &ok);

	if (! ok)
	{
		glGetShaderInfoLog(vsh, 256, nullptr, logmsg);
		std::cerr << "compilling vertex shader for char renderer failed: "
			<< logmsg << std::endl;
		abort();
	}

	GLuint fsh = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fsh, 1, &fsrc, nullptr);
	glCompileShader(fsh);
	glGetShaderiv(fsh, GL_COMPILE_STATUS, &ok);

	if (! ok)
	{
		glGetShaderInfoLog(fsh, 256, nullptr, logmsg);
		std::cerr << "compilling fragment shader for char renderer failed: "
			<< logmsg << std::endl;
		abort();
	}

	_program = glCreateProgram();
	glAttachShader(_program, vsh);
	glAttachShader(_program, fsh);
	glLinkProgram(_program);
	glGetProgramiv(_program, GL_LINK_STATUS, &ok);
	assert(ok);

	glDeleteShader(vsh);
	glDeleteShader(fsh);

	_a_coord = glGetAttribLocation(_program, "a_coord");
	_u_matrix = glGetUniformLocation(_program, "u_matrix");
	_u_color = glGetUniformLocation(_program, "u_color");

	glGenBuffers(1, &_vbo);
	_gen_vbo();
}

char_renderer::~char_renderer()
{
	glDeleteBuffers(1, &_vbo);

	for (auto data : _data_map)
		delete data.second;
}

void char_renderer::renderer_character(int code, 
									   const glm::mat4 &matrix)
{
	character_data *cd = _data_map[code];

	if (cd == nullptr)
	{
		cd = new character_data(code);
		_data_map[code] = cd;
	}

	glUseProgram(_program);

	glBindBuffer(GL_ARRAY_BUFFER, _vbo);
	glEnableVertexAttribArray(_a_coord);
	glVertexAttribPointer(_a_coord, 2, GL_FLOAT, 
						  GL_FALSE, sizeof(GLfloat) * 2, nullptr);

	glUniformMatrix4fv(_u_matrix, 1, GL_FALSE, glm::value_ptr(matrix));
	glUniform1f(_u_line_width, get_line_width());
	glUniform4fv(_u_color, 1, glm::value_ptr(get_color()));

	cd->draw();
	glDisableVertexAttribArray(_a_coord);
}

void char_renderer::set_color(glm::vec4 color)
{ _color = color; }

void char_renderer::set_line_width(float width)
{ _line_width = width; }

glm::vec4 char_renderer::get_color() const
{ return _color; }

float char_renderer::get_line_width() const
{ return _line_width; }

void char_renderer::_gen_vbo()
{
	std::vector<GLfloat> vdata;

	for (int y = 0; y < character_height; y++)
		for (int x = 0; x < character_width; x++)
		{
			float py = -static_cast<float>(y) / (character_height - 1) + 0.5;
			float px = static_cast<float>(x) / (character_width - 1) - 0.5;

			const float character_scale = 0.8;

			px *= character_scale;
			py *= character_scale;

			vdata.push_back(px);
			vdata.push_back(py);
		}

	glBindBuffer(GL_ARRAY_BUFFER, _vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * vdata.size(),
				 vdata.data(), GL_STATIC_DRAW);
}
