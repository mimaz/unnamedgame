/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#ifndef __SURFACE_ITEM_H
#define __SURFACE_ITEM_H

#include "enums.h"
#include "context-object.h"

class context;
class surface;

class surface_item : public context_object
{
  public:
	surface_item(context_object *parent);
	~surface_item();

	virtual void draw();
	virtual void on_touch_event(touch_event event);
	virtual void on_geometry_changed();
	virtual void on_pressed(int x, int y);
	virtual void on_released(int x, int y);

	virtual void set_x(int x);
	virtual void set_y(int y);
	virtual void set_width(int width);
	virtual void set_height(int height);

	bool is_inside(int x, int y) const;
	bool is_pressed() const;

	int get_x() const;
	int get_y() const;
	int get_width() const;
	int get_height() const;
	int get_left() const;
	int get_right() const;
	int get_bottom() const;
	int get_top() const;
	glm::mat4 get_matrix();
	glm::mat4 get_mvp();

  private:
	bool _pressed;
	int _x;
	int _y;
	int _width;
	int _height;
	glm::mat4 _matrix;
	bool _dirty_matrix;
};

#endif
