/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#include <std.h>

#include "surface.h"
#include "context.h"
#include "main-menu.h"
#include "surface-item.h"

LOCAL_BEGIN

enum
{
	a_coord,
	a_tex_coord,
};

const GLchar *vertex_code = R"glsl(
#version 310 es
in lowp vec2 a_coord;
in lowp vec2 a_tex_coord;

out lowp vec2 v_tex_coord;

uniform highp mat4 u_mvp;

void main()
{
	gl_Position = u_mvp * vec4(a_coord, 0.0, 1.0);

	v_tex_coord = a_tex_coord;
}
)glsl";

const GLchar *fragment_code = R"glsl(
#version 310 es

in lowp vec2 v_tex_coord;

layout(location = 0) out lowp vec4 frag_color;

uniform sampler2D u_texture;

void main()
{
	lowp vec4 tex_color = texture2D(u_texture, v_tex_coord);

	frag_color = tex_color;
}
)glsl";

const GLfloat vertex_data[] = {
	0.5, 0.5, 1, 1,
	0.5, -0.5, 1, 0,
	-0.5, -0.5, 0, 0,

	0.5, 0.5, 1, 1,
	-0.5, -0.5, 0, 0,
	-0.5, 0.5, 0, 1,
};

enum
{
	DIRTY_MATRIX = 0x1,
	DIRTY_ALL = DIRTY_MATRIX,
};

LOCAL_END

surface::surface(context *context)
	: context_part(context)
	, _context(context)
	, _width(1)
	, _height(1)
	, _dirty_flags(DIRTY_ALL)
	, _fade_progress(0)
	, _fade_flag(true)
{}

surface::~surface()
{
	glDeleteProgram(_program);
}

void surface::on_draw()
{
	get_part<main_menu>()->draw();
}

void surface::on_create()
{
	context_part::on_create();

	GLuint vsh = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vsh, 1, &vertex_code, nullptr);
	glCompileShader(vsh);

	GLuint fsh = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fsh, 1, &fragment_code, nullptr);
	glCompileShader(fsh);

	_program = glCreateProgram();
	glAttachShader(_program, vsh);
	glAttachShader(_program, fsh);
	glBindAttribLocation(_program, a_coord, "a_coord");
	glBindAttribLocation(_program, a_tex_coord, "a_tex_coord");
	glLinkProgram(_program);

	GLint ok;
	glGetProgramiv(_program, GL_LINK_STATUS, &ok);

	assert(ok);

	_u_mvp = glGetUniformLocation(_program, "u_mvp");

	glDeleteShader(vsh);
	glDeleteShader(fsh);
}

void surface::on_window_resize(int width, int height)
{
	context_part::on_window_resize(width, height);

	_width = width;
	_height = height;
	_dirty_flags |= DIRTY_MATRIX;
}

void surface::on_touch_event(touch_event event)
{
	event.x() = event.x() * get_width();
	event.y() = -event.y() * get_height();

	for (surface_item *item : _item_set)
		if (item->is_inside(event.x(), event.y())
				|| (item->is_pressed() && event == touch_event::RELEASE))
		{
			item->on_touch_event(event);
		}
}

void surface::draw_texture(GLuint texture, int x, int y, int width, int height)
{
	glDisable(GL_DEPTH_TEST);

	glm::mat4 model = glm::mat4(1);
	model = glm::translate(model, glm::vec3(x, y, 0));
	model = glm::scale(model, glm::vec3(width, height, 1));

	glm::mat4 mvp = make_mvp(model);

	glUseProgram(_program);

	glEnableVertexAttribArray(a_coord);
	glEnableVertexAttribArray(a_tex_coord);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glVertexAttribPointer(a_coord, 2, GL_FLOAT, GL_FALSE,
						  sizeof(GLfloat) * 4, vertex_data);
	glVertexAttribPointer(a_tex_coord, 2, GL_FLOAT, GL_FALSE,
						  sizeof(GLfloat) * 4, 
						  //reinterpret_cast<void *>(sizeof(GLfloat) * 2));
						  vertex_data + 2);

	glUniformMatrix4fv(_u_mvp, 1, GL_FALSE, glm::value_ptr(mvp));

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);

	glDrawArrays(GL_TRIANGLES, 0, 6);

	glDisableVertexAttribArray(a_coord);
	glDisableVertexAttribArray(a_tex_coord);
}

glm::mat4 surface::make_mvp(const glm::mat4 &model)
{ return get_matrix() * model; }

void surface::register_item(surface_item *item)
{ _item_set.insert(item); }

void surface::unregister_item(surface_item *item)
{ _item_set.erase(item); }

context *surface::get_context() const
{ return _context; }

int surface::get_width() const
{ return _width; }

int surface::get_height() const
{ return _height; }

glm::mat4 surface::get_matrix()
{
	if (_dirty_flags & DIRTY_MATRIX)
	{
		_dirty_flags &= ~DIRTY_MATRIX;

		if (get_width() > 1 && get_height() > 1)
		{
			_matrix = glm::ortho(-get_width() / 2.0f, 
								 get_width() / 2.0f, 
								 -get_height() / 2.0f, 
								 get_height() / 2.0f);

		}
		else
		{
			_matrix = glm::mat4(1);
		}
	}

	return _matrix;
}
