/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#ifndef __MENU_BUTTON_CALLBACK_H
#define __MENU_BUTTON_CALLBACK_H

class menu_button;

class menu_button_callback
{
  public:
	virtual void on_menu_button_clicked(menu_button *button) = 0;
};

#endif
