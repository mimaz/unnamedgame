/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#ifndef __STUFF_GEN_H
#define __STUFF_GEN_H

#include "context-part.h"
#include "game-machine.h"

class stuff_gen : public context_part, public game_machine::state_callback
{
  public:
	class attributes;

	stuff_gen(context *context);

	void on_player_location_changed(float location);
	void on_game_state_changed(game_state state) override;

  private:
	attributes * const _attrs;
};

#endif
