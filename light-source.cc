/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#include <std.h>

#include "light-source.h"

light_source::light_source(context_object *parent)
	: context_object(parent)
	, _position(0, 0, 0)
	, _color(1, 1, 1)
	, _brightness(1)
	, _range(10)
{}

light_source::~light_source()
{}

glm::vec3 light_source::get_position() const
{ return _position; }

glm::vec3 light_source::get_color() const
{ return _color; }

float light_source::get_brightness() const
{ return _brightness; }

float light_source::get_range() const
{ return _range; }

glm::vec3 light_source::get_real_color() const
{ return get_color() * get_brightness(); }

void light_source::set_position(glm::vec3 position)
{ _position = position; }

void light_source::set_color(glm::vec3 color)
{ _color = color; }

void light_source::set_brightness(float brightness)
{ _brightness = brightness; }

void light_source::set_range(float range)
{ _range = range; }
