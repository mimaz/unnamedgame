/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#ifndef __STUFF_H
#define __STUFF_H

#include "context.h"
#include "context-object.h"

class stuff_manager;
class path_frame;

class abstract_stuff
{
  public:
	virtual float v_get_location() const = 0;
};

class stuff : public context_object, public abstract_stuff
{
  public:
	enum
	{
		flag_delete_on_inactive = 0x1,
		flag_dirty_model = 0x2,
	};

	stuff(stuff_manager *parent);
	stuff(stuff_manager *parent, float location);

	void preprocess() override;

	virtual void draw();
	virtual bool test_for_collision(glm::vec3 point, 
									glm::vec3 *correction,
									glm::vec3 *normal);
	virtual void on_active_changed(bool active);
	virtual void on_model_matrix_changed(const glm::mat4 &matrix);

	void set_active(bool active);
	void set_path_frame(path_frame *path_frame);
	void set_matrix(const glm::mat4 &matrix);
	void set_flags(int flags);
	void reset_flags(int flags);

	bool is_active() const;
	float get_location() const;
	const path_frame *get_path_frame() const;
	glm::mat4 get_matrix() const;
	glm::mat4 get_model_matrix() const;
	int get_flags() const;
	bool is_flag_set(int flags) const;

	float v_get_location() const override;

  private:
	bool _active;
	float _location;
	const path_frame *_path_frame;
	glm::mat4 _matrix;
	glm::mat4 _model_matrix;
	int _flags;
};

#endif
