/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#ifndef __CONTEXT_OBJECT_H
#define __CONTEXT_OBJECT_H

class context;
class context_part;

class context_object
{
  public:
	enum
	{
		next_flag,
	};

	class object_registry_listener;

	static int created;
	static int destroyed;

	context_object(context *context);
	context_object(context_object *parent);
	context_object(context *context, context_object *parent);
	virtual ~context_object();

	virtual void on_create();
	virtual void on_destroy();

	virtual void preprocess();
	virtual void on_flag_changed(int flag, bool value);

	void register_child(context_object *child);
	void unregister_child(context_object *child);

	void set_flag(int flag);
	void reset_flag(int flag);
	void write_flag(int flag, bool value);

	bool is_flag_set(int flag) const;

	context *get_context() const;
	context_object *get_parent() const;
	std::size_t get_hash() const;

	  template<typename part_type>
	part_type *get_part();

	  template<typename object_type, typename ...arg_types>
	object_type *create_object(arg_types ...args);

	void delete_object(context_object *object);

  private:
	context_part *_get_part_ptr(const std::type_info &tinfo);
	void _mark_creatable_object();

	context * const _context;
	context_object * const _parent;

	std::unordered_set<context_object *> _child_set;

	int _flags;
};

class context_object::object_registry_listener
{
  public:
	virtual void on_object_registered(context_object *object) = 0;
	virtual void on_object_unregistered(context_object *object) = 0;
};

  template<typename part_type>
part_type *context_object::get_part()
{
	context_part *part = _get_part_ptr(typeid(part_type));

	if (part == nullptr)
		part = create_object<part_type>(get_context());

	return static_cast<part_type *>(part);
}

  template<typename object_type, typename ...arg_types>
object_type *context_object::create_object(arg_types ...args)
{
	_mark_creatable_object();
	object_type *object = new object_type(args...);
	object->on_create();

	return object;
}

#endif
