/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#include <std.h>

#include "stuff-gen.h"
#include "stuff-manager.h"
#include "world.h"
#include "path-line.h"
#include "player.h"
#include "rect-obstacle.h"
#include "light-box.h"
#include "tunnel.h"

class stuff_gen::attributes
{
  public:
	attributes(stuff_gen *sg)
		: sgen(sg)
		, sman(sg->get_part<stuff_manager>())
		, ctx(sg->get_context())
	{}

	stuff_gen * const sgen;
	stuff_manager * const sman;
	context * const ctx;

	float location;
	int level;
};

LOCAL_BEGIN

using attributes = stuff_gen::attributes;

float rand_float_01()
{
	return static_cast<float>(std::rand()) / RAND_MAX;
}

float rand_float_m1_1()
{
	return rand_float_01() * 2 - 1;
}

int rand_int(int mod)
{
	return std::rand() % mod;
}

__attribute__((unused))
int rand_bit()
{
	return std::rand() & 1;
}

void (*gen_functions[])(attributes *) = {
	[](attributes *attrs) -> void {
		// generate two obstacles with gap between them
		const float unit = rect_obstacle::unit_size;

		int gap;
		float shift;
		glm::vec2 size[2];
		float angles[2];

		switch (attrs->level)
		{
		  default:
			gap = rand_int(2) + 2;
			shift = 0;

			size[0] = { 
				(rand_int(4) + 4) * unit, 
				8,
			};

			size[1] = { 
				(rand_int(4) + 4) * unit, 
				8,
			};

			angles[0] = rand_float_m1_1() * PI / 2;
			angles[1] = angles[0];

			attrs->location += 5;
			break;
		}


		gap += sinf(std::max(std::abs(angles[0]), std::abs(angles[1]))) * 3;


		float offset[2] = {
			gap / 2 + size[0].x / 2 + shift / 2,
			-gap / 2 - size[1].x / 2 - shift / 2,
		};

		auto ro = attrs->sman->create_object<rect_obstacle>(attrs->sman, attrs->location);
		ro->set_flags(stuff::flag_delete_on_inactive);

		glm::mat4 matrix = glm::rotate(glm::mat4(1), angles[0], glm::vec3(0, 0, 1));
		matrix = glm::translate(matrix, glm::vec3(offset[0], 0, 0));

		ro->set_size(size[0] / unit);
		ro->set_matrix(matrix);

		ro = attrs->sman->create_object<rect_obstacle>(attrs->sman, attrs->location);
		ro->set_flags(stuff::flag_delete_on_inactive);

		matrix = glm::rotate(glm::mat4(1), angles[1], glm::vec3(0, 0, 1));
		matrix = glm::translate(matrix, glm::vec3(offset[1], 0, 0));

		ro->set_size(size[1] / unit);
		ro->set_matrix(matrix);
	},
	[](attributes *attrs) -> void {
		// generate single light box

		rgb_color colors[] = {
			rgb_color::RED,
			rgb_color::GREEN,
			rgb_color::BLUE,
			rgb_color::YELLOW,
			rgb_color::PINK,
			rgb_color::PURPLE,
			rgb_color::WHITE,
		};
		// TODO

		attrs->location += 8;

		const int color_count = sizeof(colors) / sizeof(rgb_color);

		rgb_color color = colors[rand_int(color_count)];


		auto lb = attrs->sman->create_object<light_box>(attrs->sman, attrs->location);
		lb->set_flags(stuff::flag_delete_on_inactive);
		lb->set_color(color);
	},
};

const int gen_functions_count = sizeof(gen_functions) 
							  / sizeof(gen_functions[0]);

LOCAL_END

/*
 * stuff_gen class
 */

stuff_gen::stuff_gen(context *context)
	: context_part(context)
	, _attrs(new attributes(this))
{}

void stuff_gen::on_player_location_changed(float location)
{
	location += get_part<player>()->get_view_range();

	if (location < _attrs->location)
		return;


	_attrs->location = location;
	//_attrs->level = get_part<game_state>()->get_level();

	int id = std::rand() % gen_functions_count;
	//id = 0;

	gen_functions[id](_attrs);
}

void stuff_gen::on_game_state_changed(game_state state)
{
	auto gen_menu_stuff_set = [this]() -> void {
		auto sman = get_part<stuff_manager>();


		float angle = static_cast<float>(std::rand()) / RAND_MAX * PI;

		float radius = tunnel::radius - light_box::radius * 1.1;
		float x = sinf(angle) * radius;
		float y = cosf(angle) * radius;

		glm::mat4 matrix = glm::translate(glm::mat4(1), glm::vec3(x, y, 0));
		auto lb = create_object<light_box>(sman, 9);
		lb->set_flags(stuff::flag_delete_on_inactive);
		lb->set_matrix(matrix);
		lb->set_color(rgb_color::red);

		x = cosf(angle) * radius;
		y = sinf(angle) * radius;
		matrix = glm::translate(glm::mat4(1), glm::vec3(x, y, 0));

		lb = create_object<light_box>(sman, 9);
		lb->set_flags(stuff::flag_delete_on_inactive);
		lb->set_matrix(matrix);
		lb->set_color(rgb_color::blue);
	};

	switch (state)
	{
	  case game_state::menu:
		get_part<stuff_manager>()->delete_all_stuffs();
		gen_menu_stuff_set();
		break;

	  default:
		break;
	}
}
