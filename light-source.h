/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#ifndef __LIGHT_SOURCE_H
#define __LIGHT_SOURCE_H

#include "context-object.h"

class light_source : public context_object
{
  public:
	light_source(context_object *parent);
	virtual ~light_source();

	glm::vec3 get_position() const;
	glm::vec3 get_color() const;
	float get_brightness() const;
	float get_range() const;

	glm::vec3 get_real_color() const;

	virtual void set_position(glm::vec3 position);
	virtual void set_color(glm::vec3 color);
	virtual void set_brightness(float brightness);
	virtual void set_range(float range);

  private:
	glm::vec3 _position;
	glm::vec3 _color;
	float _brightness;
	float _range;
};

#endif
