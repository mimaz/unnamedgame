/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#ifndef __CONTEXT_H
#define __CONTEXT_H

#include "enums.h"
#include "context-object.h"

class context_part;

class context : public context_object
{
  public:
	enum
	{
		flag_creatable_object = context_object::next_flag,
		flag_deletable_object,
		next_flag,
	};

	static context *create_context();
	static void delete_context(context *ctx);


	void on_create() override;

	void draw();
	void on_resize_path_frame(int width, int height);
	void on_touch_event(touch_event event);

	void register_object(context_object *object);
	void unregister_object(context_object *object);

	context_part *get_part_ptr(const std::type_info &tinfo);
	void delete_object(context_object *object);

	int get_window_width() const;
	int get_window_height() const;
	float get_wh_aspect_ratio() const;
	float get_hw_aspect_ratio() const;

	  template<typename object_type, typename ...arg_types>
	void create_object(arg_types ...args);

  private:
	using object_registry_listener = context_object::object_registry_listener;

	std::unordered_set<context_object *> _object_sett;
	std::unordered_set<object_registry_listener *> _registry_listener_set;
	std::unordered_map<std::size_t, context_part *> _part_map;

	context();
	~context();

	int _path_frame_width;
	int _path_frame_height;
	float _last_x;
	float _last_y;
};

  template<typename object_type, typename ...arg_types>
void context::create_object(arg_types ...args)
{
	static_assert(std::is_base_of<context_object, object_type>::value);

	set_flag(flag_creatable_object);
	object_type *object = new object_type(args...);
	object->on_create();

	return object;
}

#endif
