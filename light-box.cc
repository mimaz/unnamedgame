/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#include <std.h>

#include "light-box.h"
#include "world.h"
#include "player.h"
#include "light-source.h"
#include "light-group.h"

LOCAL_BEGIN

const GLchar * const vertex_main =
R"glsl(
#version 310 es
in lowp vec3 a_coord;

out highp vec3 v_coord;
out flat lowp int v_type;
out flat lowp vec3 v_normal;

uniform highp mat4 u_model;
uniform highp mat4 u_mvp;
uniform lowp vec3 u_normals[6 + 12 + 8];

const lowp int c_quads = 36;

void main()
{
	gl_Position = u_mvp * vec4(a_coord, 1.0);
	v_coord = vec3(u_model * vec4(a_coord, 1.0));

	lowp int triangle_id = (gl_VertexID - 1) / 3;

	// detect provoking vertex
	if (gl_VertexID % 3 == 2)
	{
		lowp int normal_id = min(triangle_id, c_quads) / 2;

		if (triangle_id > c_quads)
			normal_id += min(triangle_id - c_quads, 8);

		lowp vec3 normal = u_normals[normal_id];
		normal = vec3(u_model * vec4(normal, 0.0));

		if (triangle_id < 12)
			v_type = 0;
		else if (triangle_id < 12 + 24)
			v_type = 1;
		else
			v_type = 2;

		v_normal = normalize(normal);
	}
}
)glsl";

const GLchar * const fragment_main =
R"(
#version 310 es
in highp vec3 v_coord;
in flat lowp int v_type;
in flat lowp vec3 v_normal;

layout(location=0) out lowp vec4 frag_color;

uniform lowp vec3 u_colors[3];
uniform highp vec3 u_camera;
uniform highp float u_view_range;

void main()
{
	highp vec3 player_vector = u_camera - v_coord;
	highp float player_distance = length(player_vector);
	lowp float fog = (u_view_range - player_distance) / u_view_range;

	lowp vec3 color;

	if (v_type == 0)
	{
		color = u_colors[0] * fog;
	}
	else
	{
		lowp float cosine = dot(v_normal, player_vector) 
						  / player_distance;

		lowp float factor = cosine * fog;

		color = u_colors[v_type] * factor;
	}

	frag_color = vec4(color, 1.0);
}
)";

const int a_coord = 0;
const int a_mode = 1;

const float SIZE = 0.5;

const float M = SIZE / 5;
const float L = -SIZE / 2;
const float R = SIZE / 2;
const float B = -SIZE / 2;
const float T = SIZE / 2;
const float N = -SIZE / 2;
const float F = SIZE / 2;
const float LM = L + M;
const float RM = R - M;
const float BM = B + M;
const float TM = T - M;
const float NM = N + M;
const float FM = F - M;

class vertex_attrs
{
  public:
	GLfloat x, y, z;
};

const vertex_attrs vdata[] = {
	// faces
	// front
	{ RM, TM, N },
	{ RM, BM, N },
	{ LM, BM, N },

	{ RM, TM, N },
	{ LM, BM, N },
	{ LM, TM, N },

	// back
	{ LM, TM, F },
	{ LM, BM, F },
	{ RM, BM, F },

	{ LM, TM, F },
	{ RM, BM, F },
	{ RM, TM, F },
	
	// left
	{ L, TM, NM },
	{ L, BM, NM },
	{ L, BM, FM },

	{ L, TM, NM },
	{ L, BM, FM },
	{ L, TM, FM },

	// right
	{ R, TM, FM },
	{ R, BM, FM },
	{ R, BM, NM },

	{ R, TM, FM },
	{ R, BM, NM },
	{ R, TM, NM },

	// bottom
	{ RM, B, NM },
	{ RM, B, FM },
	{ LM, B, FM },

	{ RM, B, NM },
	{ LM, B, FM },
	{ LM, B, NM },
	
	// top
	{ RM, T, FM },
	{ RM, T, NM },
	{ LM, T, NM },

	{ RM, T, FM },
	{ LM, T, NM },
	{ LM, T, FM },

	// front edges
	// top
	{ LM, TM, N },
	{ LM, T, NM },
	{ RM, T, NM },

	{ LM, TM, N },
	{ RM, T, NM },
	{ RM, TM, N },

	// right
	{ RM, TM, N },
	{ R, TM, NM },
	{ R, BM, NM },

	{ RM, TM, N },
	{ R, BM, NM },
	{ RM, BM, N },

	// bottom
	{ RM, BM, N },
	{ RM, B, NM },
	{ LM, B, NM },

	{ RM, BM, N },
	{ LM, B, NM },
	{ LM, BM, N },
	
	// left
	{ LM, TM, N },
	{ L, TM, NM },
	{ L, BM, NM },

	{ LM, TM, N },
	{ L, BM, NM },
	{ LM, BM, N },

	// back edges
	// top
	{ LM, TM, F },
	{ RM, TM, F },
	{ RM, T, FM },

	{ LM, TM, F },
	{ RM, T, FM },
	{ LM, T, FM },

	// right
	{ RM, TM, F },
	{ RM, BM, F },
	{ R, BM, FM },

	{ RM, TM, F },
	{ R, BM, FM },
	{ R, TM, FM },
	
	// bottom
	{ LM, BM, F },
	{ LM, B, FM },
	{ RM, B, FM },

	{ LM, BM, F },
	{ RM, B, FM },
	{ RM, BM, F },

	// left
	{ L, TM, FM },
	{ L, BM, FM },
	{ LM, BM, F },

	{ L, TM, FM },
	{ LM, BM, F },
	{ LM, TM, F },

	// central edges
	// top-left
	{ L, TM, NM },
	{ L, TM, FM },
	{ LM, T, FM },

	{ L, TM, NM },
	{ LM, T, FM },
	{ LM, T, NM },

	// top-right
	{ R, TM, NM },
	{ RM, T, NM },
	{ RM, T, FM },

	{ R, TM, NM },
	{ RM, T, FM },
	{ R, TM, FM },

	// bottom-right
	{ R, BM, NM },
	{ R, BM, FM },
	{ RM, B, FM },

	{ R, BM, NM },
	{ RM, B, FM },
	{ RM, B, NM },

	// bottom-left
	{ L, BM, NM },
	{ LM, B, NM },
	{ LM, B, FM },

	{ L, BM, NM },
	{ LM, B, FM },
	{ L, BM, FM },

	// corners
	// front
	{ LM, TM, N },
	{ L, TM, NM },
	{ LM, T, NM },

	{ RM, TM, N },
	{ RM, T, NM },
	{ R, TM, NM },

	{ RM, BM, N },
	{ R, BM, NM },
	{ RM, B, NM },
	
	{ LM, BM, N },
	{ LM, B, NM },
	{ L, BM, NM },

	// back
	{ LM, TM, F },
	{ LM, T, FM },
	{ L, TM, FM },

	{ RM, TM, F },
	{ R, TM, FM },
	{ RM, T, FM },

	{ RM, BM, F },
	{ R, BM, FM },
	{ RM, B, FM },

	{ LM, BM, F },
	{ L, BM, FM },
	{ LM, B, FM },
};

const glm::vec3 normal(float x, float y, float z)
{ return glm::normalize(glm::vec3(x, y, z)); }

const glm::vec3 base_normals[] = {
	// faces
	normal(0, 0, -1),
	normal(0, 0, 1),
	normal(-1, 0, 0),
	normal(1, 0, 0),
	normal(0, -1, 0),
	normal(0, 1, 0),
	
	// edges
	normal(0, 1, -1),
	normal(1, 0, -1),
	normal(0, -1, -1),
	normal(-1, 0, -1),

	normal(0, 1, 1),
	normal(1, 0, 1),
	normal(0, -1, 1),
	normal(-1, 0, 1),

	normal(-1, 1, 0),
	normal(1, 1, 0),
	normal(1, -1, 0),
	normal(-1, -1, 0),

	// corners
	normal(-1, 1, -1),
	normal(1, 1, -1),
	normal(1, -1, -1),
	normal(-1, -1, -1),

	normal(-1, 1, 1),
	normal(1, 1, 1),
	normal(1, -1, 1),
	normal(-1, -1, 1),
};

static GLchar msg[256];

enum 
{
	VBO,
	BUFF_CNT
};

class box_renderer : public context_part
{
  public:
	box_renderer(context *context)
		: context_part(context)
	{
		GLuint vsh = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vsh, 1, &vertex_main, nullptr);
		glCompileShader(vsh);

		GLint ok;
		glGetShaderiv(vsh, GL_COMPILE_STATUS, &ok);

		if (! ok)
		{
			glGetShaderInfoLog(vsh, 256, nullptr, msg);
			std::cerr << "compilling vertex shader for light-box failed: "
				<< msg << std::endl;
			abort();
		}

		GLuint fsh = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(fsh, 1, &fragment_main, nullptr);
		glCompileShader(fsh);
		
		glGetShaderiv(fsh, GL_COMPILE_STATUS, &ok);

		if (! ok)
		{
			glGetShaderInfoLog(fsh, 256, nullptr, msg);
			std::cerr << "compilling fragment shader for"
				" liquid light box failed: " << msg << std::endl;
			abort();
		}

		program = glCreateProgram();
		glAttachShader(program, vsh);
		glAttachShader(program, fsh);

		glBindAttribLocation(program, a_coord, "a_coord");
		glBindAttribLocation(program, a_mode, "a_mode");

		glLinkProgram(program);

		glGetProgramiv(program, GL_LINK_STATUS, &ok);

		if (! ok)
		{
			glGetProgramInfoLog(program, 256, nullptr, msg);
			std::cerr << "linking program for liquid light-box"
				" failed: " << msg << std::endl;
			abort();
		}

		glDeleteShader(vsh);
		glDeleteShader(fsh);


		u_model = glGetUniformLocation(program, "u_model");
		u_mvp = glGetUniformLocation(program, "u_mvp");
		u_normals = glGetUniformLocation(program, "u_normals");
		u_colors = glGetUniformLocation(program, "u_colors");
		u_camera = glGetUniformLocation(program, "u_camera");
		u_view_range = glGetUniformLocation(program, "u_view_range");


		glGenBuffers(BUFF_CNT, buffers);
		glBindBuffer(GL_ARRAY_BUFFER, buffers[VBO]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vdata), 
					 vdata, GL_STATIC_DRAW);
	}

	~box_renderer()
	{
		glDeleteProgram(program);
		glDeleteBuffers(BUFF_CNT, buffers);
	}

	GLuint program;
	GLuint buffers[BUFF_CNT];
	GLuint u_model;
	GLuint u_mvp;
	GLuint u_normals;
	GLuint u_colors;
	GLuint u_camera;
	GLuint u_view_range;
};

glm::vec3 color_v3(rgb_color color, float P, float S)
{
	glm::vec3 rgb = color;

	return {
		std::max(rgb.r * P, S),
		std::max(rgb.g * P, S),
		std::max(rgb.b * P, S),
	};
}

glm::vec3 liquid_color_v3(rgb_color color)
{ return color_v3(color, 1.0, 0.7); }

glm::vec3 corner_color_v3(rgb_color color)
{ return color_v3(color, 0.8, 0.5); }

glm::vec3 edge_color_v3(rgb_color color)
{ return { 0.6, 0.6, 0.6 }; }

glm::vec3 light_color_v3(rgb_color color)
{ return color_v3(color, 1.0, 0.0); }

LOCAL_END

/*
 * light_box class
 */

const float light_box::radius = SIZE;

light_box::light_box(stuff_manager *parent)
	: light_box(parent, 0)
{}

light_box::light_box(stuff_manager *parent, float location)
	: stuff(parent, location)
	, _light(create_object<light_source>(this))
	, _angle(static_cast<float>(std::rand()) / RAND_MAX)
{
	_light->set_range(8);

	get_part<world>()->get_light_group()->register_light(_light);
}

light_box::~light_box()
{}

void light_box::draw()
{
	//std::cout << "light pos: " << _light->get_position() << std::endl;
	stuff::draw();
	_angle += 0.03;

	box_renderer *rd = get_part<box_renderer>();

	glEnable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	glUseProgram(rd->program);

	glBindBuffer(GL_ARRAY_BUFFER, rd->buffers[VBO]);

	glEnableVertexAttribArray(a_coord);
	glEnableVertexAttribArray(a_mode);

	glVertexAttribPointer(a_coord, 3, GL_FLOAT, GL_FALSE, 
						  sizeof(vertex_attrs), nullptr);

	glVertexAttribPointer(a_mode, 1, GL_UNSIGNED_BYTE, GL_FALSE,
						  sizeof(vertex_attrs), reinterpret_cast<void *>(sizeof(GLfloat) * 3));

	glm::mat4 rotation = glm::mat4(1);
	rotation = glm::rotate(rotation, _angle * 4, glm::vec3(0, 0, 1));
	rotation = glm::rotate(rotation, _angle * 2, glm::vec3(0, 1, 0));
	rotation = glm::rotate(rotation, _angle, glm::vec3(1, 0, 0));

	glm::mat4 model = get_model_matrix() * rotation;
	glm::mat4 mvp = get_part<world>()->make_mvp_matrix(model);

	glUniformMatrix4fv(rd->u_model, 1, GL_FALSE, glm::value_ptr(model));
	glUniformMatrix4fv(rd->u_mvp, 1, GL_FALSE, glm::value_ptr(mvp));
	glUniform3fv(rd->u_camera, 1, glm::value_ptr(get_part<player>()->get_matrix() * glm::vec4(0, 0, 0, 1)));
	glUniform1f(rd->u_view_range, get_part<player>()->get_view_range());
	glUniform3fv(rd->u_normals, sizeof(base_normals) / sizeof(glm::vec3), 
				 reinterpret_cast<const GLfloat *>(base_normals));

	glEnable(GL_CULL_FACE);
	glDisable(GL_CULL_FACE);
	glCullFace(GL_FRONT);
	glDisable(GL_BLEND);

	glm::vec3 colors[] = {
		liquid_color_v3(get_color()),
		edge_color_v3(get_color()),
		corner_color_v3(get_color()),
	};

	glUniform3fv(rd->u_colors, 3, reinterpret_cast<GLfloat *>(colors));
	
	glDrawArrays(GL_TRIANGLES, 0, 
				 sizeof(vdata) / sizeof(vertex_attrs) * 3);

	glDisableVertexAttribArray(a_coord);
	glDisableVertexAttribArray(a_mode);
}

void light_box::on_active_changed(bool active)
{
	stuff::on_active_changed(active);

	if (active)
		get_part<world>()->get_light_group()->register_light(get_light());
	else
		get_part<world>()->get_light_group()->unregister_light(get_light());
}

void light_box::on_model_matrix_changed(const glm::mat4 &matrix)
{
	stuff::on_model_matrix_changed(matrix);

	_light->set_position(matrix * glm::vec4(0, 0, 0, 1));
}

void light_box::set_color(rgb_color color)
{ 
	_color = color; 
	_light->set_color(light_color_v3(color));
}

const light_source *light_box::get_light() const
{ return _light; }

rgb_color light_box::get_color() const
{ return _color; }
