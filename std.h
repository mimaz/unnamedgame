/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#ifndef __STD_H
#define __STD_H

#include <iostream>
#include <memory>
#include <thread>
#include <vector>
#include <algorithm>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <queue>
#include <list>
#include <array>
#include <string>
#include <cstdlib>
#include <cstdio>
#include <cassert>

#define GLFW_INCLUDE_ES3
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

constexpr float PI = static_cast<float>(M_PI);

inline std::ostream &operator<<(std::ostream &os, glm::vec2 vec)
{
	return os << "[" << vec.x << ", " << vec.y << "]";
}

inline std::ostream &operator<<(std::ostream &os, glm::vec3 vec)
{
	return os << "[" << vec.x << ", " << vec.y << ", " << vec.z << "]";
}

inline std::ostream &operator<<(std::ostream &os, glm::vec4 vec)
{
	return os << "[" << vec.x << ", " << vec.y << ", " << vec.z << ", " << vec.w << "]";
}

inline std::ostream &operator<<(std::ostream &os, glm::mat4 mat)
{
	return os << mat[0] << std::endl << mat[1] << std::endl << mat[2] << std::endl << mat[3];
}

#define LOCAL_BEGIN namespace {
#define LOCAL_END }

#endif
