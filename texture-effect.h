/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#ifndef __TEXTURE_EFFECT_H
#define __TEXTURE_EFFECT_H

#include "context-part.h"

class texture_effect : public context_part
{
  public:
	texture_effect(context *context);
	~texture_effect();

	void draw_with_blur_effect(GLuint texture,
							   float exposure);
	void draw_with_blur_effect(GLuint texture,
							   const glm::mat4 &matrix,
							   float exposure);
};

#endif
