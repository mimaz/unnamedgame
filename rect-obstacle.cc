/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#include <std.h>

#include "rect-obstacle.h"
#include "dynamic-lighting.h"
#include "world.h"
#include "player.h"

LOCAL_BEGIN

const GLchar * const vertex_code = 
R"(
#version 310 es
in lowp vec3 a_coord;
in lowp vec3 a_normal;
in lowp float a_mode;
in mediump vec2 a_tex_coord;

uniform highp mat4 u_model;
uniform highp mat4 u_mvp;
uniform mediump vec2 u_size;

out mediump vec2 v_tex_coord;
out highp vec3 v_coord;
out lowp vec3 v_normal;
out lowp float v_mode;

void main()
{
	mediump vec4 coord4 = vec4(a_coord, 1.0);
	lowp vec4 normal4 = vec4(-a_normal, 0.0);

	coord4.xy *= u_size;

	gl_Position = u_mvp * coord4;

	v_tex_coord = a_tex_coord * u_size;
	v_coord = vec3(u_model * coord4);
	v_normal = vec3(u_model * normal4);
	v_mode = a_mode;
}
)";

const GLchar * const fragment_code =
R"(
uniform sampler2D u_texture;
uniform highp vec3 u_camera;
uniform highp float u_view_range;

in mediump vec2 v_tex_coord;
in highp vec3 v_coord;
in lowp vec3 v_normal;
in lowp float v_mode;

layout(location = 0) out lowp vec4 frag_color;

void main()
{
	lowp vec3 normal = normalize(v_normal);

	lowp vec4 color;

	if (v_mode > 0.0)
		color = texture2D(u_texture, v_tex_coord);
	else
		color = vec4(0.6, 0.6, 0.6, 1.0);

	lowp vec3 light = calculate_diffuse_light(v_coord, normal,
											  u_camera, u_view_range);

	color.rgb *= light;

	frag_color = color;
}
)";

/*
 * vertex layout:
 * float[3] coordinates
 * float[3] normal
 * float[1] mode: 1=texture; -1=flat color
 * float[2] tex_coord
 */

const float U = 0.1;

const GLfloat vdata[] = {
	// front face
	U, U, U, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0,
	U, -U, U, 0.0, 0.0, 1.0, 1.0, 1.0, 0.0,
	-U, -U, U, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0,

	U, U, U, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0,
	-U, -U, U, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0,
	-U, U, U, 0.0, 0.0, 1.0, 1.0, 0.0, 1.0,

	// back face
	-U, U, -U, 0.0, 0.0, -1.0, 1.0, 1.0, 1.0,
	-U, -U, -U, 0.0, 0.0, -1.0, 1.0, 1.0, 0.0,
	U, -U, -U, 0.0, 0.0, -1.0, 1.0, 0.0, 0.0,

	-U, U, -U, 0.0, 0.0, -1.0, 1.0, 1.0, 1.0,
	U, -U, -U, 0.0, 0.0, -1.0, 1.0, 0.0, 0.0,
	U, U, -U, 0.0, 0.0, -1.0, 1.0, 0.0, 1.0,

	// left side
	-U, U, U, -1.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	-U, -U, U, -1.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	-U, -U, -U, -1.0, 0.0, 0.0, 0.0, 0.0, 0.0,

	-U, U, U, -1.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	-U, -U, -U, -1.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	-U, U, -U, -1.0, 0.0, 0.0, 0.0, 0.0, 0.0,

	// right side
	U, U, U, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	U, -U, -U, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	U, -U, U, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0,

	U, U, U, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	U, U, -U, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0,
	U, -U, -U, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0,

	// top side
	-U, U, U, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0,
	-U, U, -U, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0,
	U, U, -U, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0,

	-U, U, U, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0,
	U, U, -U, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0,
	U, U, U, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0,

	// bottom side
	U, -U, U, 0.0, -1.0, 0.0, 0.0, 0.0, 0.0,
	U, -U, -U, 0.0, -1.0, 0.0, 0.0, 0.0, 0.0,
	-U, -U, -U, 0.0, -1.0, 0.0, 0.0, 0.0, 0.0,

	U, -U, U, 0.0, -1.0, 0.0, 0.0, 0.0, 0.0,
	-U, -U, -U, 0.0, -1.0, 0.0, 0.0, 0.0, 0.0,
	-U, -U, U, 0.0, -1.0, 0.0, 0.0, 0.0, 0.0,
};

const int TEXTURE_WIDTH = 2;
const int TEXTURE_HEIGHT = 2;

GLubyte *gen_texture_data()
{
	GLubyte *data = new GLubyte[TEXTURE_WIDTH * TEXTURE_HEIGHT * 4];

	for (int y = 0; y < TEXTURE_HEIGHT; y++)
		for (int x = 0; x < TEXTURE_WIDTH; x++)
		{
			GLubyte *ptr = data + (y * TEXTURE_WIDTH + x) * 4;

			glm::vec3 yellow = glm::vec3(0.8, 0.8, 0.0);
			glm::vec3 black = glm::vec3(0.2, 0.2, 0.2);

			glm::vec3 color = (x + y) & 1 ? yellow : black;

			*ptr++ = color.r * 0xFF;
			*ptr++ = color.g * 0xFF;
			*ptr++ = color.b * 0xFF;
			*ptr = 0xFF;
		}

	return data;
}

class shader : public dynamic_lighting::shader
{
  public:
	static dynamic_lighting::shader *create()
	{ return new shader; }

	void resolve() override
	{
		dynamic_lighting::shader::resolve();

		a_coord = glGetAttribLocation(program, "a_coord");
		a_normal = glGetAttribLocation(program, "a_normal");
		a_mode = glGetAttribLocation(program, "a_mode");
		a_tex_coord = glGetAttribLocation(program, "a_tex_coord");
		u_model = glGetUniformLocation(program, "u_model");
		u_mvp = glGetUniformLocation(program, "u_mvp");
		u_size = glGetUniformLocation(program, "u_size");
		u_camera = glGetUniformLocation(program, "u_camera");
		u_view_range = glGetUniformLocation(program, "u_view_range");
	}

	GLuint a_coord;
	GLuint a_normal;
	GLuint a_mode;
	GLuint a_tex_coord;
	GLuint u_model;
	GLuint u_mvp;
	GLuint u_size;
	GLuint u_camera;
	GLuint u_view_range;
};

class rect_obstacle_renderer : public context_part
{
  public:
	rect_obstacle_renderer(context *ctx)
		: context_part(ctx)
	{
		auto light_group = get_part<world>()->get_light_group();

		lighting = dynamic_lighting::build()
			.vertex_source(1, &vertex_code)
			.fragment_source(1, &fragment_code)
			.shader_allocator(shader::create)
			.light_source_group(light_group)
			.parent(this)
			.done();

		glGenBuffers(1, &vbo);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vdata),
					 vdata, GL_STATIC_DRAW);

		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

		GLubyte *texture_data = gen_texture_data();

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, TEXTURE_WIDTH, TEXTURE_HEIGHT,
					 0, GL_RGBA, GL_UNSIGNED_BYTE, texture_data);

		delete texture_data;
	}

	~rect_obstacle_renderer()
	{
		glDeleteTextures(1, &texture);
		glDeleteBuffers(1, &vbo);
	}

	dynamic_lighting *lighting;
	GLuint vbo;
	GLuint texture;
};

LOCAL_END

const float rect_obstacle::unit_size = U * 2;

/*
 * rect_obstacle class
 */

rect_obstacle::rect_obstacle(stuff_manager *parent)
	: rect_obstacle(parent, 0)
{}

rect_obstacle::rect_obstacle(stuff_manager *parent, float location)
	: stuff(parent, location)
	, _size(1, 1)
{}

rect_obstacle::~rect_obstacle()
{}

void rect_obstacle::draw()
{
	stuff::draw();

	rect_obstacle_renderer *rd = get_part<rect_obstacle_renderer>();

	glm::vec3 obstacle_position = get_model_matrix() * glm::vec4(0, 0, 0, 1);
	shader *sh = static_cast<shader *>(rd->lighting->use_program(obstacle_position));

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);

	glEnableVertexAttribArray(sh->a_coord);
	glEnableVertexAttribArray(sh->a_normal);
	glEnableVertexAttribArray(sh->a_mode);
	glEnableVertexAttribArray(sh->a_tex_coord);


	glm::mat4 model = get_model_matrix();
	glm::mat4 mvp = get_part<world>()->make_mvp_matrix(model);

	glUniformMatrix4fv(sh->u_model, 1, GL_FALSE, glm::value_ptr(model));
	glUniformMatrix4fv(sh->u_mvp, 1, GL_FALSE, glm::value_ptr(mvp));
	glUniform2fv(sh->u_size, 1, glm::value_ptr(get_size()));
	glUniform3fv(sh->u_camera, 1, glm::value_ptr(get_part<player>()->get_matrix() * glm::vec4(0, 0, 0, 1)));
	glUniform1f(sh->u_view_range, get_part<player>()->get_view_range());


	glBindTexture(GL_TEXTURE_2D, rd->texture);

	glBindBuffer(GL_ARRAY_BUFFER, rd->vbo);

	glVertexAttribPointer(sh->a_coord, 3, GL_FLOAT, GL_FALSE,
						  sizeof(GLfloat) * 9, nullptr);
	glVertexAttribPointer(sh->a_normal, 3, GL_FLOAT, GL_FALSE,
						  sizeof(GLfloat) * 9,
						  reinterpret_cast<void *>(sizeof(GLfloat) * 3));
	glVertexAttribPointer(sh->a_mode, 1, GL_FLOAT, GL_FALSE,
						  sizeof(GLfloat) * 9,
						  reinterpret_cast<void *>(sizeof(GLfloat) * 6));
	glVertexAttribPointer(sh->a_tex_coord, 2, GL_FLOAT, GL_FALSE,
						  sizeof(GLfloat) * 9,
						  reinterpret_cast<void *>(sizeof(GLfloat) * 7));

	glDrawArrays(GL_TRIANGLES, 0, sizeof(vdata) / sizeof(GLfloat) / 9);

	glDisableVertexAttribArray(sh->a_coord);
	glDisableVertexAttribArray(sh->a_normal);
	glDisableVertexAttribArray(sh->a_mode);
	glDisableVertexAttribArray(sh->a_tex_coord);
}

bool rect_obstacle::test_for_collision(glm::vec3 point, 
									   glm::vec3 *correction, 
									   glm::vec3 *normal)
{
	return false;

	// TODO
	glm::mat4 inv_model = glm::inverse(get_model_matrix());

	glm::vec4 local_4 = inv_model * glm::vec4(point, 1.0);
	glm::vec3 local = glm::vec3(local_4);

	const float 
		W = U * get_size().x * 2,
		H = U * get_size().y * 2,
		D = U * 2,
		left = -W / 2,
		right = W / 2,
		bottom = -H / 2,
		top = H / 2,
		far = -D / 2,
		near = D / 2;

	bool collision = local.x > left
		&& local.x < right
		&& local.y > bottom
		&& local.y < top
		&& local.z > far
		&& local.z < near;

	if (! collision)
		return false;

	struct def
	{
		glm::vec3 normal;
		glm::vec3 correction;
		float distance;
	};

	def defs[] = {
		{ { -1, 0, 0 }, { left, local.y, local.z }, local.x - left },
		{ { 1, 0, 0 }, { right, local.y, local.z }, right - local.x },
		{ { 0, -1, 0 }, { local.x, bottom, local.z }, local.y - bottom },
		{ { 0, 1, 0 }, { local.x, top, local.z }, top - local.y },
		{ { 0, 0, -1 }, { local.x, local.y, far }, local.z - far },
		{ { 0, 0, 1 }, { local.x, local.y, near }, near - local.z },
	};

	def best = { { 0, 0, 0 }, { 0, 0, 0 }, 1000 };

	for (auto def : defs)
		if (std::abs(def.distance) < std::abs(best.distance))
			best = def;

	glm::vec4 local_normal = glm::vec4(best.normal, 0.0);
	glm::vec4 local_correction = glm::vec4(best.correction, 1.0);

	*normal = glm::vec3(get_model_matrix() * local_normal);
	*correction = glm::vec3(get_model_matrix() * local_correction);

	return true;
}

void rect_obstacle::set_size(glm::vec2 size)
{ _size = size; }

glm::vec2 rect_obstacle::get_size() const
{ return _size; }
