/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#ifndef __BUFFERED_TEXT_H
#define __BUFFERED_TEXT_H

#include "context.h"
#include "context-object.h"

class text_renderer;

class buffered_text : public context_object
{
  public:
	buffered_text(context_object *parent);
	~buffered_text();

	void set_text(const std::string &text);
	void set_text_size(float size);
	void set_size(int width, int height);

	GLuint get_texture_handle();

	std::string get_text() const;
	float get_text_size() const;
	int get_width() const;
	int get_height() const;

  private:
	void _redraw();

	GLuint _path_framebuffer;
	GLuint _texture;

	std::string _text;
	float _text_size;
	int _width;
	int _height;
	int _flags;
};

#endif
