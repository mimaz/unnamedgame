/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#include <std.h>

#include "text-renderer.h"
#include "char-renderer.h"
#include "surface.h"

class char_matrix
{
  public:
	char_matrix()
		: char_matrix(0, glm::mat4(1))
	{}

	char_matrix(int code, const glm::mat4 &matrix)
		: code(code), matrix(matrix)
	{}

	int code;
	glm::mat4 matrix;
};

static int measure_text(const text_renderer *renderer,
						char_matrix *char_data)
{
	class char_position
	{
	  public:
		char_position()
			: char_position(0, 0, 0, 0)
		{}

		char_position(int code, float x, float y, float lw)
			: code(code), x(x), y(y), lw(lw)
		{}

		int code;
		float x;
		float y;
		float lw;
	};

	const float character_width = renderer->get_text_size() * 5 / 7;
	const float character_height = renderer->get_text_size();


	std::vector<char_position> position_list;


	float x_pos = 0;
	float y_pos = 0;
	float line_width = 0;
	float max_line_width = 0;
	int char_index = 0;


	auto commit_line = [&]() -> void {
		auto it_guard = position_list.rbegin() + char_index;

		for (auto it = position_list.rbegin(); it != it_guard; it++)
			it->lw = line_width;

		max_line_width = std::max(line_width, max_line_width);


		x_pos = 0;
		y_pos += character_height;
		line_width = 0;
		char_index = 0;
	};



	for (char c : renderer->get_text())
	{
		if (c == '\n')
		{
			commit_line();
		}
		else if (c >= ' ')
		{
			position_list.emplace_back(
					static_cast<int>(c),
					x_pos,
					y_pos,
					0
			);

			x_pos += character_width;
			line_width += character_width;
			char_index++;
		}
	}


	commit_line();



	const float text_width = max_line_width;
	const float text_height = y_pos;

	char_matrix *data_ptr = char_data;



	for (char_position cp : position_list)
	{
		float real_x = cp.x;
		float real_y = text_height - character_height - cp.y;
		float line_width = cp.lw;

		switch (renderer->get_vertical_alignment())
		{
		  case vertical_alignment::BOTTOM:
			real_y += character_height / 2
				- renderer->get_surface_height() / 2;
			break;

		  case vertical_alignment::TOP:
			real_y += character_height / 2
				+ renderer->get_surface_height() / 2
				- text_height;
			break;

		  case vertical_alignment::CENTER:
			real_y += character_height /2
				- text_height / 2;
			break;
		}

		switch (renderer->get_horizontal_alignment())
		{
		  case horizontal_alignment::LEFT:
			real_x += character_width / 2
				- renderer->get_surface_width() / 2;
			break;

		  case horizontal_alignment::RIGHT:
			real_x += renderer->get_surface_width() / 2
				- text_width
				+ character_width / 2
				+ (text_width - line_width);
			break;

		  case horizontal_alignment::CENTER:
			real_x += character_width / 2
				- text_width / 2
				+ (text_width / line_width) / 2;
			break;
		}


		glm::mat4 matrix = glm::mat4(1);
		matrix = glm::translate(matrix, glm::vec3(real_x, real_y, 0));
		matrix = glm::scale(matrix, glm::vec3(character_width, character_height, 1));


		*data_ptr++ = char_matrix(cp.code, matrix);
	}


	return data_ptr - char_data;
}

text_renderer::text_renderer(context *context)
	: context_part(context)
	, _width(100)
	, _height(100)
	, _size(10)
	, _line_width(5)
	, _color(1, 1, 1, 1)
	, _valign(vertical_alignment::CENTER)
	, _halign(horizontal_alignment::CENTER)
	, _text("DEFAULT")
{}

void text_renderer::set_surface_width(float width)
{ _width = width; }

void text_renderer::set_surface_height(float height)
{ _height = height; }

void text_renderer::set_text_size(float size)
{ _size = size; }

void text_renderer::set_line_width(float width)
{ _line_width = width; }

void text_renderer::set_max_line_width()
{ set_line_width(999); }

void text_renderer::set_text_color(glm::vec4 color)
{ _color = color; }

void text_renderer::set_vertical_alignment(vertical_alignment align)
{ _valign = align; }

void text_renderer::set_horizontal_alignment(horizontal_alignment align)
{ _halign = align; }

void text_renderer::set_text(const std::string &text)
{ _text = text; }

void text_renderer::render()
{
	glm::mat4 proj = glm::ortho(-get_surface_width() / 2,
								get_surface_width() / 2,
								-get_surface_height() / 2,
								get_surface_height() / 2);

	render(proj);
}

void text_renderer::render(const glm::mat4 &matrix)
{
	char_matrix data[get_text().length()];
	int count = measure_text(this, data);

	get_part<char_renderer>()->set_color(get_text_color());
	get_part<char_renderer>()->set_line_width(get_line_width());

	for (int i = 0; i < count; i++)
	{
		glm::mat4 mvp = matrix * data[i].matrix;

		get_part<char_renderer>()->renderer_character(data[i].code, mvp);
	}
}

float text_renderer::get_surface_width() const
{ return _width; }

float text_renderer::get_surface_height() const
{ return _height; }

float text_renderer::get_text_size() const
{ return _size; }

float text_renderer::get_line_width() const
{ return _line_width; }

glm::vec4 text_renderer::get_text_color() const
{ return _color; }

vertical_alignment text_renderer::get_vertical_alignment() const
{ return _valign; }

horizontal_alignment text_renderer::get_horizontal_alignment() const
{ return _halign; }

std::string text_renderer::get_text() const
{ return _text; }
