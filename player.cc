/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#include <std.h>

#include "player.h"
#include "world.h"
#include "light-source.h"
#include "path-line.h"
#include "path-frame.h"
#include "stuff-gen.h"
#include "gradient-gyroscope.h"

LOCAL_BEGIN

enum gyroscope_type
{
	YAW,
	PITCH,
	ROLL,
};

  template<gyroscope_type type>
class player_gyroscope : public context_part, public gradient_gyroscope
{
  public:
	player_gyroscope(context *context)
		: context_part(context)
	{}

	void rotate(float angle) override
	{
		switch (type)
		{
		  case YAW:
			get_part<player>()->rotate_view(angle, glm::vec3(0, 1, 0));
			break;

		  case PITCH:
			get_part<player>()->rotate_view(angle, glm::vec3(1, 0, 0));
			break;

		  case ROLL:
			get_part<player>()->rotate_view(angle, glm::vec3(0, 0, 1));
			break;
		}
	}

	glm::vec3 get_axis() override
	{
		switch (type)
		{
		  case YAW:
			return get_matrix() * glm::vec4(0, 1, 0, 0);

		  case PITCH:
			return get_matrix() * glm::vec4(1, 0, 0, 0);

		  case ROLL:
			return get_matrix() * glm::vec4(0, 0, 1, 0);
		}
	}

	glm::vec3 get_vector() override
	{
		switch (type)
		{
		  case YAW:
		  case PITCH:
			return get_matrix() * glm::vec4(0, 0, 1, 0);

		  case ROLL:
			return get_matrix() * glm::vec4(0, 1, 0, 0);
		}
	}

	glm::vec3 get_target() override
	{
		switch (type)
		{
		  case YAW:
		  case PITCH:
			return get_part<player>()->get_follow_vector();

		  default:
			return glm::vec3(0, 1, 0);
		}
	}

	glm::mat4 get_matrix() 
	{
		return get_part<player>()->get_matrix();
	}
};

LOCAL_END

/*
 * player class
 */

player::player(context *context)
	: context_part(context)
	, _light(create_object<light_source>(this))
	, _speed(0)
	, _target_speed(0)
	, _view_range(30)
	, _matrix(1)
	, _path_frame(nullptr)
	, _follow_vector(0, 0, 1)
	, _flags(0)
{
	get_light()->set_range(30);
	get_light()->set_color({ 1, 1, 1 });
}

void player::on_create()
{
	context_part::on_create();
}

void player::set_matrix(const glm::mat4 &matrix)
{
	_matrix = matrix;

	get_light()->set_position(matrix * glm::vec4(0, 0, 0, 1));
	get_part<world>()->dirty_view();
}

void player::set_target_speed(float speed)
{ _target_speed = speed; }

void player::set_view_range(float range)
{ 
	_view_range = range; 
	get_part<world>()->dirty_view();
}

void player::set_flag(flags flag)
{
	_flags |= flag;
}

void player::reset_flag(flags flag)
{
	_flags &= ~flag;
}

void player::rotate_view(float angle, glm::vec3 axis)
{
	_matrix *= glm::rotate(glm::mat4(1), angle, axis);

	get_part<world>()->dirty_view();
}

void player::draw()
{
	/*
	 * update path_frame pointer
	 */
	if (get_path_frame() == nullptr)
		_path_frame = get_part<path_line>()->get_first_path_frame();

	auto new_path_frame = get_path_frame()->closer_one(get_matrix() * glm::vec4(0, 0, 0, 1));

	if (new_path_frame != _path_frame)
	{
		_path_frame = new_path_frame;
		get_part<stuff_gen>()->on_player_location_changed(get_path_frame()->get_location());
	}


	_speed += (get_target_speed() - get_speed()) / 12;


	/*
	 * move player
	 */
	glm::vec3 pos_off = get_matrix() 
		* glm::vec4(0, 0, 1, 0) 
		* get_speed() / 60.0f;


	std::vector<std::vector<glm::vec3>::iterator> erased;

	/*
	for (auto it = _push_list.begin(); it != _push_list.end(); it++)
	{
		glm::vec3 psh = *it;

		if (psh.x + psh.y + psh.z < 0.1 / 60)
			erased.push_back(it);

		pos_off += psh;

		*it = psh * 0.95f;
	}

	for (auto it : erased)
		_push_list.erase(it);
		*/

	glm::mat4 new_matrix = glm::mat4(1);
	new_matrix = glm::translate(new_matrix, pos_off);
	new_matrix *= get_matrix();

	set_matrix(new_matrix);


	if (is_flag_set(flag_autopilot))
	{
		float prec = get_speed() / 5;

		const path_frame *local = get_path_frame();
		const path_frame *target = local;

		if (get_target_speed() > 0.001)
		{
			while (target && target->get_location() < local->get_location() + prec)
				target = target->get_next();

			if (target)
			{
				glm::vec3 direction = target->get_matrix() * glm::vec4(0, 0, 0, 1)
					- get_matrix() * glm::vec4(0, 0, 0, 1);

				direction = glm::normalize(direction);

				_follow_vector = direction;

				get_part<player_gyroscope<YAW>>()->tick();
				get_part<player_gyroscope<PITCH>>()->tick();
				get_part<player_gyroscope<ROLL>>()->tick();
			}
		}
	}
}

void player::reflect(glm::vec3 normal)
{
	// TODO do it better
	
	push_player(normal * get_speed() * 2.0f);
}

void player::push_player(glm::vec3 momentum)
{ _push_list.push_back(momentum / 60.0f); }

light_source *player::get_light() const
{ return _light; }

float player::get_speed() const
{ return _speed; }

float player::get_target_speed() const
{ return _target_speed; }

float player::get_view_range() const
{ return _view_range; }

glm::mat4 player::get_matrix() const
{ return _matrix; }

const path_frame *player::get_path_frame() const
{ return _path_frame; }

glm::vec3 player::get_follow_vector() const
{ return _follow_vector; }

int player::get_flags() const
{ return _flags; }

bool player::is_flag_set(flags flag) const
{ return get_flags() & flag; }
