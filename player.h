/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#ifndef __PLAYER_H
#define __PLAYER_H

#include "enums.h"
#include "context-part.h"

class path_frame;
class light_source;
class gradient_gyroscope;

class player : public context_part
{
  public:
	enum flags
	{
		flag_autopilot = context_part::next_flag,
		next_flag,
	};

	player(context *context);

	void on_create() override;

	void set_matrix(const glm::mat4 &matrix);
	void set_target_speed(float target);
	void set_view_range(float range);
	void set_flag(flags flag);
	void reset_flag(flags flag);
	void rotate_view(float angle, glm::vec3 axis);
	void draw();
	void reflect(glm::vec3 normal);
	void push_player(glm::vec3 normal);

	light_source *get_light() const;
	float get_speed() const;
	float get_target_speed() const;
	float get_view_range() const;
	glm::mat4 get_matrix() const;
	const path_frame *get_path_frame() const;
	glm::vec3 get_follow_vector() const;
	int get_flags() const;
	bool is_flag_set(flags flag) const;

  private:
	light_source * const _light;

	float _speed;
	float _target_speed;
	float _view_range;
	glm::mat4 _matrix;
	const path_frame *_path_frame;
	glm::vec3 _follow_vector;
	std::vector<glm::vec3> _push_list;
	int _flags;
};

#endif
