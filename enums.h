/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#ifndef __ENUMS_H
#define __ENUMS_H

class movement
{
  public:
	enum id_type
	{
		FORWARD,
		BACKWARD,
		LEFTWARD,
		RIGHTWARD,
		DOWNWARD,
		UPWARD,
		ENUM_SIZE,
	};

	movement();
	movement(const movement &other) = default;
	movement(movement &&other) = default;
	movement(id_type id);

	movement &operator=(const movement &other) = default;
	movement &operator=(movement &&other) = default;
	movement &operator=(id_type id);

	operator int() const;
	operator std::string() const;

  private:
	id_type _id;
};

class rgb_color
{
  public:
	enum id_type
	{
		BLACK,
		WHITE,
		RED,
		GREEN,
		BLUE,
		YELLOW,
		PURPLE,
		PINK,
		ENUM_SIZE,
	};

	static const rgb_color black;
	static const rgb_color white;
	static const rgb_color red;
	static const rgb_color green;
	static const rgb_color blue;
	static const rgb_color yellow;
	static const rgb_color purple;
	static const rgb_color pink;

	rgb_color();
	rgb_color(const rgb_color &other) = default;
	rgb_color(rgb_color &&other) = default;
	rgb_color(id_type id);

	rgb_color &operator=(const rgb_color &other) = default;
	rgb_color &operator=(rgb_color &&other) = default;
	rgb_color &operator=(id_type id);

	operator int() const;
	operator glm::vec3() const;
	operator glm::vec4() const;
	operator std::string() const;

  private:
	id_type _id;
};

class touch_event
{
  public:
	enum id_type
	{
		PRESS,
		MOVE,
		RELEASE,
		ENUM_SIZE,
	};

	touch_event();
	touch_event(const touch_event &other) = default;
	touch_event(touch_event &&other) = default;
	touch_event(id_type id);
	touch_event(id_type id, glm::vec2 pos);

	touch_event &operator=(const touch_event &other) = default;
	touch_event &operator=(touch_event &&other) = default;
	touch_event &operator=(id_type id);
	touch_event &operator=(glm::vec2 pos);

	float &x();
	float &y();
	float x() const;
	float y() const;

	operator int() const;
	operator glm::vec2() const;
	operator std::string() const;

  private:
	id_type _id;
	glm::vec2 _pos;
};

class game_state
{
  public:
	enum id_type
	{
		none,
		menu,
		play,
	};

	game_state();
	game_state(const game_state &other) = default;
	game_state(game_state &&other) = default;
	game_state(id_type id);

	game_state &operator=(const game_state &other) = default;
	game_state &operator=(game_state &&other) = default;
	game_state &operator=(id_type id);

	std::string to_string() const;

	operator int() const;
	operator std::string() const;

  private:
	id_type _id;
};

enum class vertical_alignment
{
	CENTER,
	BOTTOM,
	TOP,
};

enum class horizontal_alignment
{
	CENTER,
	LEFT,
	RIGHT,
};

#endif
