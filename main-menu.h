/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#ifndef __MAIN_MENU
#define __MAIN_MENU

#include "context-part.h"
#include "menu-button-callback.h"

class menu_button;

class main_menu : public context_part, public menu_button_callback
{
  public:
	enum
	{
		flag_varying_exposure = context_part::next_flag,
		next_flag,
	};

	main_menu(context *context);
	~main_menu();

	void draw();
	void preprocess() override;
	void on_create() override;
	void on_flag_changed(int flag, bool value) override;
	void on_window_resize(int width, int height) override;
	void on_menu_button_clicked(menu_button *button) override;

	void set_target_exposure(float exposure);

	GLuint get_texture_handle() const;
	float get_exposure() const;
	float get_target_exposure() const;

  private:
	void _switch_visible();
	void _switch_story();
	void _switch_hidden();

	menu_button * const _start_button;
	menu_button * const _story_button;
	menu_button * const _achievements_button;
	menu_button * const _leaderboards_button;

	float _exposure;
	float _target_exposure;

	GLuint _path_framebuffer;
	GLuint _texture;
};

#endif
