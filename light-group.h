/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#ifndef __LIGHT_GROUP_H
#define __LIGHT_GROUP_H

#include "context-object.h"

class light_source;

class light_group : public context_object
{
  public:
	light_group(context_object *parent);
	~light_group();

	void register_light(const light_source *light);
	void unregister_light(const light_source *light);

	int get_light_count() const;
	int read_all_sources(const light_source **array) const;
	int read_reachable_sources(glm::vec3 point, 
							   const light_source **array) const;

  private:
	void _calculate_reachable();

	std::unordered_set<const light_source *> _light_set;
};

#endif
