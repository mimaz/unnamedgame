/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#include <std.h>

#include "enums.h"

movement::movement()
	: movement(FORWARD)
{}

movement::movement(movement::id_type id)
	: _id(id)
{}

movement &movement::operator=(id_type id)
{ _id = id; return *this; }

movement::operator int() const
{ return static_cast<int>(_id); }

movement::operator std::string() const
{
	switch (_id)
	{
	  case FORWARD:
		return "FORWARD";

	  case BACKWARD:
		return "BACKWARD";

	  case LEFTWARD:
		return "LEFTWARD";

	  case RIGHTWARD:
		return "RIGHTWARD";

	  case DOWNWARD:
		return "DOWNWARD";

	  case UPWARD:
		return "UPWARD";

	  default:
		abort();
	}
}

/*
 * rgb_color
 */

const rgb_color rgb_color::black(BLACK);
const rgb_color rgb_color::white(WHITE);
const rgb_color rgb_color::red(RED);
const rgb_color rgb_color::green(GREEN);
const rgb_color rgb_color::blue(BLUE);
const rgb_color rgb_color::yellow(YELLOW);
const rgb_color rgb_color::purple(PURPLE);
const rgb_color rgb_color::pink(PINK);

rgb_color::rgb_color()
	: rgb_color(BLACK)
{}

rgb_color::rgb_color(id_type id)
	: _id(id)
{}

rgb_color &rgb_color::operator=(id_type id)
{ _id = id; return *this; }

rgb_color::operator int() const
{ return _id; }

rgb_color::operator glm::vec3() const
{
	switch (_id)
	{
	  case BLACK:
		return { 0, 0, 0 };

	  case WHITE:
		return { 1, 1, 1 };

	  case RED:
		return { 1, 0, 0 };

	  case GREEN:
		return { 0, 1, 0 };

	  case BLUE:
		return { 0, 0, 1 };

	  case YELLOW:
		return { 1, 1, 0 };

	  case PURPLE:
		return { 1, 0, 1 };

	  case PINK:
		return { 1, 1, 0 };

	  default:
		abort();
	}
}

rgb_color::operator glm::vec4() const
{
	return glm::vec4(operator glm::vec3(), 1.0);
}

rgb_color::operator std::string() const
{
	switch (_id)
	{
	  case BLACK:
		return "BLACK";

	  case WHITE:
		return "WHITE";

	  case RED:
		return "RED";

	  case GREEN:
		return "GREEN";

	  case BLUE:
		return "BLUE";

	  case YELLOW:
		return "YELLOW";

	  case PURPLE:
		return "PURPLE";

	  case PINK:
		return "PINK";

	  default:
		abort();
	}
}

/*
 * touch_event
 */

touch_event::touch_event()
	: touch_event(RELEASE)
{}

touch_event::touch_event(id_type id)
	: touch_event(id, glm::vec2(0, 0))
{}

touch_event::touch_event(id_type id, glm::vec2 pos)
	: _id(id)
	, _pos(pos)
{}

touch_event &touch_event::operator=(id_type id)
{
	_id = id;
	return *this;
}

touch_event &touch_event::operator=(glm::vec2 pos)
{
	_pos = pos;
	return *this;
}

float &touch_event::x() 
{ return _pos.x; }

float &touch_event::y()
{ return _pos.y; }

float touch_event::x() const
{ return _pos.x; }

float touch_event::y() const
{ return _pos.y; }

touch_event::operator int() const
{ return static_cast<int>(_id); }

touch_event::operator glm::vec2() const
{ return _pos; }

touch_event::operator std::string() const
{
	switch (_id)
	{
	  case PRESS:
		return "PRESS";

	  case MOVE:
		return "MOVE";

	  case RELEASE:
		return "RELEASE";

	  default:
		abort();
	}
}

/*
 * game_state
 */

game_state::game_state()
	: game_state(none)
{}

game_state::game_state(id_type id)
	: _id(id)
{}

game_state &game_state::operator=(id_type id)
{ 
	_id = id; 
	return *this;
}

std::string game_state::to_string() const
{ return static_cast<std::string>(*this); }

game_state::operator int() const
{ return _id; }

game_state::operator std::string() const
{
	switch (_id)
	{
	  case none:
		return "none";

	  case menu:
		return "menu";

	  case play:
		return "play";

	  default:
		abort();
	}
}
