/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#include <std.h>

#include "stuff.h"
#include "stuff-manager.h"
#include "path-frame.h"

/*
 * stuff class
 */

stuff::stuff(stuff_manager *parent)
	: stuff(parent, 0)
{}

stuff::stuff(stuff_manager *parent, float location)
	: context_object(parent)
	, _location(location)
	, _path_frame(nullptr)
	, _matrix(1)
	, _model_matrix(1)
	, _flags(0)
{}

void stuff::preprocess()
{
	context_object::preprocess();

	if (is_flag_set(flag_dirty_model))
	{
		reset_flags(flag_dirty_model);

		if (get_path_frame())
			_model_matrix = get_path_frame()->get_matrix() * get_matrix();
		else
			_model_matrix = get_matrix();

		on_model_matrix_changed(get_model_matrix());
	}
}

void stuff::draw()
{}

bool stuff::test_for_collision(glm::vec3 point, glm::vec3 *correction, glm::vec3 *normal)
{ return false; }

void stuff::set_path_frame(path_frame *path_frame)
{ 
	_path_frame = path_frame; 
	set_flags(flag_dirty_model);
}

void stuff::on_active_changed(bool active)
{
	set_flags(flag_dirty_model);
}

void stuff::on_model_matrix_changed(const glm::mat4 &matrix)
{}

void stuff::set_active(bool active)
{
	_active = active;

	on_active_changed(active);
}

void stuff::set_matrix(const glm::mat4 &matrix)
{
	_matrix = matrix;
	_flags |= flag_dirty_model;
}

void stuff::set_flags(int flags)
{
	_flags |= flags;
}

void stuff::reset_flags(int flags)
{
	_flags &= ~flags;
}

bool stuff::is_active() const
{ return _active; }

float stuff::get_location() const
{ return _location; }

const path_frame *stuff::get_path_frame() const
{ return _path_frame; }

glm::mat4 stuff::get_matrix() const
{ return _matrix; }

glm::mat4 stuff::get_model_matrix() const
{ return _model_matrix; }

int stuff::get_flags() const
{ return _flags; }

bool stuff::is_flag_set(int flags) const
{ return get_flags() & flags; }

float stuff::v_get_location() const 
{ return get_location(); }
