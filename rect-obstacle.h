/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#ifndef __RECT_OBSTACLE_H
#define __RECT_OBSTACLE_H

#include "stuff.h"

class dynamic_lighting;

class rect_obstacle : public stuff
{
  public:
	static const float unit_size;

	rect_obstacle(stuff_manager *parent);
	rect_obstacle(stuff_manager *parent, float location);
	~rect_obstacle();

	void draw() override;
	bool test_for_collision(glm::vec3 point, 
							glm::vec3 *correction, 
							glm::vec3 *normal) override;

	void set_size(glm::vec2 size);

	glm::vec2 get_size() const;

  private:
	glm::vec2 _size;
};

#endif
