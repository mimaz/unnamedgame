/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#include <std.h>

#include "menu-button.h"
#include "buffered-text.h"
#include "text-renderer.h"
#include "surface.h"
#include "context.h"

LOCAL_BEGIN

const int a_coord = 0;
const int a_tex_coord = 1;

const GLchar *vertex_code = R"glsl(
#version 310 es

in lowp vec2 a_coord;
in lowp vec2 a_tex_coord;

out lowp vec2 v_coord;
out lowp vec2 v_tex_coord;

uniform highp mat4 u_matrix;
uniform mediump float u_wh_ratio;

void main()
{
	gl_Position = u_matrix * vec4(a_coord, 0.0, 1.0);

	v_coord = vec2(a_coord.x * u_wh_ratio, a_coord.y);
	v_tex_coord = a_tex_coord;
}
)glsl";

const GLchar *fragment_code = R"glsl(
#version 310 es

in lowp vec2 v_coord;
in lowp vec2 v_tex_coord;

out lowp vec4 frag_color;

uniform sampler2D u_text_texture;
uniform lowp vec2 u_center[2];
uniform lowp float u_progress_pressed[2];
uniform lowp float u_progress_released[2];
uniform highp float u_wibe_angle;
uniform mediump float u_wh_ratio;

const highp float c_pi = 3.14159;

bool is_brighter(lowp int index)
{
	lowp vec2 coord = v_coord;

	lowp vec2 diff = u_center[index] - coord;
	mediump float dist = diff.x * diff.x + diff.y * diff.y;

	mediump float diagonal = 1.0 + u_wh_ratio * u_wh_ratio;

	lowp float progress = dist / diagonal;

	lowp float pressed = u_progress_pressed[index];
	lowp float released = u_progress_released[index];

	return pressed > progress && released < progress;
}

void main()
{
	lowp vec2 tex_coord = v_tex_coord;

	bool bright = is_brighter(0) || is_brighter(1);

	// TODO wibe
	lowp vec4 text_color = texture2D(u_text_texture, tex_coord);
	lowp vec4 background_color = vec4(0.4, 0.4, 0.4, 0.6);
	lowp vec4 pressed_color = vec4(1.0, 0.0, 0.0, 0.1);
	lowp vec4 normal_text_color = vec4(0.4, 1.0, 0.4, 1.0);
	lowp vec4 pressed_text_color = vec4(1.0, 1.0, 1.0, 1.0);

	lowp vec4 color;

	if (text_color.a > 0.01)
	{
		color = bright ? pressed_text_color : normal_text_color;
	}
	else
	{
		color = bright ? pressed_color : background_color;
	}

	frag_color = color;
}
)glsl";

const GLfloat vertices[] = {
	0.5, 0.5, 1, 1,
	0.5, -0.5, 1, 0,
	-0.5, -0.5, 0, 0,

	0.5, 0.5, 1, 1,
	-0.5, -0.5, 0, 0,
	-0.5, 0.5, 0, 1,
};

class menu_button_renderer : public context_part
{
  public:
	menu_button_renderer(context *context)
		: context_part(context)
	{
		glGenBuffers(1, &vbo);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices),
					 vertices, GL_STATIC_DRAW);


		static GLchar logmsg[256];
		GLint ok;

		GLuint vsh = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vsh, 1, &vertex_code, nullptr);
		glCompileShader(vsh);
		glGetShaderiv(vsh, GL_COMPILE_STATUS, &ok);

		if (! ok)
		{
			glGetShaderInfoLog(vsh, 256, nullptr, logmsg);
			std::cerr << "compilling vertex shader failed: "
				<< logmsg << std::endl;
			abort();
		}

		GLuint fsh = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(fsh, 1, &fragment_code, nullptr);
		glCompileShader(fsh);
		glGetShaderiv(fsh, GL_COMPILE_STATUS, &ok);

		if (! ok)
		{
			glGetShaderInfoLog(fsh, 256, nullptr, logmsg);
			std::cerr << "compilling fragment shader failed: "
				<< logmsg << std::endl;
			abort();
		}

		program = glCreateProgram();
		glAttachShader(program, vsh);
		glAttachShader(program, fsh);
		glBindAttribLocation(program, a_coord, "a_coord");
		glBindAttribLocation(program, a_tex_coord, "a_tex_coord");
		glLinkProgram(program);

		u_matrix = glGetUniformLocation(program, "u_matrix");
		u_center = glGetUniformLocation(program, "u_center");
		u_progress_pressed = glGetUniformLocation(program, "u_progress_pressed");
		u_progress_released = glGetUniformLocation(program, "u_progress_released");
		u_wibe_angle = glGetUniformLocation(program, "u_wibe_angle");
		u_wh_ratio = glGetUniformLocation(program, "u_wh_ratio");

		glGetProgramiv(program, GL_LINK_STATUS, &ok);
		assert(ok);

		glDeleteShader(vsh);
		glDeleteShader(fsh);
	}

	~menu_button_renderer()
	{
		glDeleteBuffers(1, &vbo);
	}

	GLuint vbo;
	GLuint program;
	GLuint u_matrix;
	GLuint u_center;
	GLuint u_progress_pressed;
	GLuint u_progress_released;
	GLuint u_wibe_angle;
	GLuint u_wh_ratio;
};

enum press_mode
{
	PRESS,
	RELEASE,
	FREE,
};

float get_progress(int path_frames, int max)
{
	return std::min(max, path_frames) / static_cast<float>(max);
}

float get_pressed_progress(int path_frames)
{
	return get_progress(path_frames, 90);
}

float get_released_progress(int path_frames)
{
	return get_progress(path_frames, 60);
}

LOCAL_END

/*
 * menu_button class
 */

menu_button::menu_button(context_object *parent)
	: surface_item(parent)
	, _buffered_text(create_object<buffered_text>(this))
	, _id(0)
	, _touch_id(0)
	, _wibe_angle(0)
{
	for (int &f : _path_frames_pressed)
		f = 0;

	for (int &m : _mode)
		m = FREE;
}

menu_button::~menu_button()
{}

void menu_button::on_geometry_changed()
{
	surface_item::on_geometry_changed();

	_buffered_text->set_size(get_width(), get_height());
}

void menu_button::on_touch_event(touch_event event)
{
	surface_item::on_touch_event(event);

	float x = (event.x() - get_x() + get_width() / 2) / get_width();
	float y = (event.y() - get_y() + get_height() / 2) / get_height();

	_center[_touch_id] = glm::vec2(x - 0.5, y - 0.5);

	switch (event)
	{
	  case touch_event::PRESS:
		_path_frames_pressed[_touch_id] = 0;
		_path_frames_released[_touch_id] = 0;
		_mode[_touch_id] = PRESS;
		break;

	  case touch_event::RELEASE:
		_mode[_touch_id] = RELEASE;

		_touch_id++;
		_touch_id %= 2;

		if (is_inside(event.x(), event.y()))
		{
			for (menu_button_callback *cb : _callback_set)
				cb->on_menu_button_clicked(this);
		}
		break;

	  default:
		break;
	}
}

void menu_button::draw()
{
	surface_item::draw();

	_wibe_angle += 0.1;

	menu_button_renderer *rd = get_part<menu_button_renderer>();

	glEnable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);

	glUseProgram(rd->program);

	glEnableVertexAttribArray(a_coord);
	glEnableVertexAttribArray(a_tex_coord);

	glBindBuffer(GL_ARRAY_BUFFER, rd->vbo);
	glVertexAttribPointer(a_coord, 2, GL_FLOAT, GL_FALSE, 
						  sizeof(GLfloat) * 4, nullptr);
	glVertexAttribPointer(a_tex_coord, 2, GL_FLOAT, GL_FALSE,
						  sizeof(GLfloat) * 4, 
						  reinterpret_cast<void *>(sizeof(GLfloat) * 2));

	glm::mat4 mvp = get_mvp();

	glm::vec2 center[] = {
		glm::vec2(_center[0].x * get_width() / get_height(), _center[0].y),
		glm::vec2(_center[1].x * get_width() / get_height(), _center[1].y),
	};

	GLfloat progress_pressed[] = {
		get_pressed_progress(_path_frames_pressed[0]),
		get_pressed_progress(_path_frames_pressed[1]),
	};

	GLfloat progress_released[] = {
		get_released_progress(_path_frames_released[0]),
		get_released_progress(_path_frames_released[1]),
	};

	for (int i = 0; i < 2; i++)
		switch (_mode[i])
		{
		  case PRESS:
			_path_frames_pressed[i]++;
			break;

		  case RELEASE:
			if (progress_released[i] >= progress_pressed[i])
			{
				_mode[i] = FREE;
			}
			else
			{
				_path_frames_pressed[i]++;

				if (_path_frames_pressed[i] > 4)
					_path_frames_released[i]++;
			}
			break;

		  case FREE:
			break;
		}

	glUniformMatrix4fv(rd->u_matrix, 1, GL_FALSE, glm::value_ptr(mvp));
	glUniform2fv(rd->u_center, 2, glm::value_ptr(center[0]));
	glUniform1fv(rd->u_progress_pressed, 2, progress_pressed);
	glUniform1fv(rd->u_progress_released, 2, progress_released);
	glUniform1f(rd->u_wibe_angle, _wibe_angle);
	glUniform1f(rd->u_wh_ratio, static_cast<float>(get_width()) / get_height());

	glBindTexture(GL_TEXTURE_2D, _buffered_text->get_texture_handle());

	glDrawArrays(GL_TRIANGLES, 0, 6);

	glDisableVertexAttribArray(a_coord);
	glDisableVertexAttribArray(a_tex_coord);
}

void menu_button::register_callback(menu_button_callback *cb)
{ _callback_set.insert(cb); }

void menu_button::unregister_callback(menu_button_callback *cb)
{ _callback_set.erase(cb); }

void menu_button::set_text(const std::string &text)
{ _buffered_text->set_text(text); }

void menu_button::set_text_size(float size)
{ _buffered_text->set_text_size(size); }

void menu_button::set_id(int id)
{ _id = id; }

std::string menu_button::get_text() const
{ return _buffered_text->get_text(); }

float menu_button::get_text_size() const
{ return _buffered_text->get_text_size(); }

int menu_button::get_id() const
{ return _id; }
