/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#ifndef __TUNNEL_H
#define __TUNNEL_H

#include "context-part.h"

class path_frame;
class light_source;
class dynamic_lighting;

class tunnel : public context_part
{
  public:
	static const float radius;
	static const float segment_length;

	class shader;
	class lighting;

	tunnel(context *context);
	~tunnel();

	void draw();

	dynamic_lighting *get_lighting() const;

  private:
	void _gen_coord_data();

	dynamic_lighting * const _lighting;
	GLuint * const _buffers;
};

#endif
