/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#include <std.h>

#include "path-gen.h"
#include "world.h"
#include "player.h"
#include "path-line.h"
#include "light-source.h"
#include "tunnel.h"
#include "path-frame.h"

LOCAL_BEGIN

path_line_gen::shape generate_straight_line(float length)
{
	path_line_gen::shape sh;

	sh.count = length / tunnel::segment_length;
	sh.angle = 0;
	sh.axis = { 0, 0, 1 };

	return sh;
}

/*
path_line_gen::shape generate_shape(path_line_gen *gen)
{
	path_line_gen::shape sh;

	int level = 1;

	glm::vec3 axis;
	float angle;
	float length;

	switch (level)
	{
	  case 0:
		// straight
		axis = { 0, 0, 1 };
		angle = 0;
		length = 5;
		break;

	  default:
		// some curves
	  {
		glm::vec3 axes[] = {
			{ 0, 1, 0 },
			{ 0, -1, 0 },
			{ 1, 0, 0 },
			{ -1, 0, 0 },
		};

		axis = axes[std::rand() % 4];
		angle = PI * std::rand() / RAND_MAX / 8;
		length = 4;
	  }
	}

	sh.count = length / tunnel::segment_length;
	sh.angle = angle / sh.count;
	sh.axis = axis;

	return sh;
}
*/

LOCAL_END

/*
 * path_line_gen class
 */

path_line_gen::path_line_gen(context *context)
	: context_part(context)
	, _shape_generator(generate_straight_line)
{}

void path_line_gen::on_game_state_changed(game_state state)
{
	reset_shape();

	switch (state)
	{
	  case game_state::menu:
		_shape_generator = generate_straight_line;
		break;

	  default:
		break;
	}
}

void path_line_gen::preprocess()
{
	context_part::preprocess();

	if (get_part<player>()->get_path_frame() == nullptr)
		return;

	float player_location = get_part<player>()->get_path_frame()->get_location();
	float render_distance = get_part<player>()->get_view_range() + 10;
	path_line *pth = get_part<path_line>();


	auto distance_to_first = [pth, player_location]() -> float {
		float distance = pth->get_first_path_frame()->get_location()
			- player_location;

		return std::abs(distance);
	};

	auto distance_to_last = [pth, player_location]() -> float {
		float distance = pth->get_last_path_frame()->get_location()
			- player_location;

		return std::abs(distance);
	};


	while (distance_to_last() < render_distance)
	{
		if (_shape.count < 1)
			_shape = _shape_generator(30);

		_shape.count--;

		glm::mat4 matrix = glm::rotate(glm::mat4(1), 
				_shape.angle, _shape.axis);

		get_part<path_line>()->emplace_path_frame_back(matrix);
	}


	const float range_margin = 5;

	while (distance_to_first() > render_distance + range_margin)
		pth->delete_path_frame_front();
}

void path_line_gen::reset_shape()
{
	_shape.count = 0;
}
