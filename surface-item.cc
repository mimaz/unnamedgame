/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#include <std.h>

#include "surface-item.h"
#include "context.h"
#include "surface.h"

surface_item::surface_item(context_object *parent)
	: context_object(parent)
	, _pressed(false)
	, _x(0)
	, _y(0)
	, _width(0)
	, _height(0)
	, _matrix(0)
	, _dirty_matrix(true)
{
	get_part<surface>()->register_item(this);
}

surface_item::~surface_item()
{
	get_part<surface>()->unregister_item(this);
}

void surface_item::draw()
{}

void surface_item::on_touch_event(touch_event event)
{
	switch (event)
	{
	  case touch_event::PRESS:
		_pressed = true;
		on_pressed(event.x(), event.y());
		break;

	  case touch_event::RELEASE:
		_pressed = false;
		on_released(event.x(), event.y());
		break;

	  default:
		break;
	}
}

void surface_item::on_geometry_changed()
{ 
	_dirty_matrix = true; 
}

void surface_item::on_pressed(int x, int y)
{}

void surface_item::on_released(int x, int y)
{}

void surface_item::set_x(int x)
{
	_x = x;
	on_geometry_changed();
}

void surface_item::set_y(int y)
{
	_y = y;
	on_geometry_changed();
}

void surface_item::set_width(int width)
{
	_width = width;
	on_geometry_changed();
}

void surface_item::set_height(int height)
{
	_height = height;
	on_geometry_changed();
}

bool surface_item::is_inside(int x, int y) const
{
	return x >= get_left()
		&& x <= get_right()
		&& y >= get_bottom()
		&& y <= get_top();
}

bool surface_item::is_pressed() const
{ return _pressed; }

int surface_item::get_x() const
{ return _x; }

int surface_item::get_y() const
{ return _y; }

int surface_item::get_width() const
{ return _width; }

int surface_item::get_height() const
{ return _height; }

int surface_item::get_left() const
{ return get_x() - get_width() / 2; }

int surface_item::get_right() const
{ return get_x() + get_width() / 2; }

int surface_item::get_bottom() const
{ return get_y() - get_height() / 2; }

int surface_item::get_top() const
{ return get_y() + get_height() / 2; }

glm::mat4 surface_item::get_matrix()
{
	if (_dirty_matrix)
	{
		_dirty_matrix = false;

		_matrix = glm::scale(
				glm::translate(glm::mat4(1), glm::vec3(get_x(), get_y(), 0)),
				glm::vec3(get_width(), get_height(), 1));
	}

	return _matrix;
}

glm::mat4 surface_item::get_mvp()
{ return get_part<surface>()->make_mvp(get_matrix()); }
