/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#include <std.h>

#include "buffered-text.h"
#include "text-renderer.h"

enum
{
	DIRTY_NONE = 0x0,
	DIRTY_SIZE = 0x1,
	DIRTY_TEXT = 0x2,
	DIRTY_ALL = DIRTY_SIZE
		| DIRTY_TEXT,
};

buffered_text::buffered_text(context_object *parent)
	: context_object(parent)
	, _text("DEFAULT")
	, _text_size(10)
	, _width(1)
	, _height(1)
	, _flags(DIRTY_ALL)
{
	glGenTextures(1, &_texture);
	glGenFramebuffers(1, &_path_framebuffer);

	glBindTexture(GL_TEXTURE_2D, _texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, get_width(), get_height(), 0, 
				 GL_RGBA, GL_UNSIGNED_BYTE, nullptr);

	glBindFramebuffer(GL_FRAMEBUFFER, _path_framebuffer);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, 
						   GL_TEXTURE_2D, _texture, 0);

	GLenum draw_buffers[] = {
		GL_COLOR_ATTACHMENT0,
	};

	glDrawBuffers(1, draw_buffers);

	assert(glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

buffered_text::~buffered_text()
{
	glDeleteFramebuffers(1, &_path_framebuffer);
	glDeleteTextures(1, &_texture);
}

void buffered_text::set_size(int width, int height)
{
	_width = width;
	_height = height;

	_flags |= DIRTY_SIZE;
}

void buffered_text::set_text(const std::string &text)
{
	_text = text;
	_flags |= DIRTY_TEXT;
}

void buffered_text::set_text_size(float size)
{
	_text_size = size;
	_flags |= DIRTY_TEXT;
}

GLuint buffered_text::get_texture_handle() 
{
	_redraw();

	return _texture;
}

std::string buffered_text::get_text() const
{ return _text; }

float buffered_text::get_text_size() const
{ return _text_size; }

int buffered_text::get_width() const
{ return _width; }

int buffered_text::get_height() const
{ return _height; }

void buffered_text::_redraw()
{
	if (_flags == DIRTY_NONE
			|| get_width() < 2
			|| get_height() < 2)
	{
		return;
	}

	if (_flags & DIRTY_TEXT)
		_flags &= ~DIRTY_TEXT;

	if (_flags & DIRTY_SIZE)
	{
		_flags &= ~DIRTY_SIZE;

		glBindTexture(GL_TEXTURE_2D, _texture);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, get_width(), get_height(), 0, 
					 GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
	}


	glBindFramebuffer(GL_FRAMEBUFFER, _path_framebuffer);
	glViewport(0, 0, get_width(), get_height());


	text_renderer *tr = get_part<text_renderer>();
	tr->set_surface_width(get_width());
	tr->set_surface_height(get_height());
	tr->set_text_size(get_text_size());
	tr->set_line_width(20);
	tr->set_text(get_text());
	tr->render();

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}
