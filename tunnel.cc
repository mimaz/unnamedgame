/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#include <std.h>

#include "tunnel.h"
#include "world.h"
#include "path-line.h"
#include "path-frame.h"
#include "player.h"
#include "dynamic-lighting.h"
#include "light-source.h"

LOCAL_BEGIN

#define QUALITY 30
#define RANDOM_DATA_SETS 8
#define a_coord 0
#define FRAMES_PER_CALL 50

const int TRIANGLES = QUALITY * 2;
const float MAX_ANGLE = PI / QUALITY;

#define STRINGIFY_(x) #x
#define STRINGIFY(x) STRINGIFY_(x)

static const GLchar * const vertex_code = 
"#version 310 es\n"
"const lowp int c_quality = " STRINGIFY(QUALITY) ";\n"
"const lowp int c_random_data_sets = " STRINGIFY(RANDOM_DATA_SETS) ";\n"
"const lowp int c_max_path_frames = " STRINGIFY(FRAMES_PER_CALL) ";\n"
"layout(location = " STRINGIFY(a_coord) ") in lowp vec2 a_coord;\n"

R"(
const lowp int c_max_data_sets = c_max_path_frames + 1;

uniform highp mat4 u_models[c_max_data_sets];
uniform highp mat4 u_mvps[c_max_data_sets];
uniform highp int u_hash_ids[c_max_data_sets];
uniform lowp float u_randoms[c_random_data_sets * c_quality];

out highp vec3 v_coord;
out highp vec3 v_pivot;
out lowp vec3 v_color;
out lowp float v_random;

void main()
{
	lowp int data_index = gl_InstanceID + gl_VertexID % 2;
	
	highp mat4 model = u_models[data_index];
	highp mat4 mvp = u_mvps[data_index];
	lowp float random = u_randoms[data_index];
	highp int hash_id = u_hash_ids[data_index];

	highp vec4 coord = vec4(a_coord, 0.0, 1.0);

	lowp int random_id = hash_id * c_quality + gl_VertexID / 2 - 1;

	v_coord = vec3(model * coord);
	v_pivot = vec3(model * vec4(0.0, 0.0, 0.0, 1.0));
	v_random = u_randoms[random_id];

	gl_Position = mvp * coord;
}
)";

static const GLchar * const fragment_code = R"(
const lowp int c_max_path_frames = 100;

uniform highp vec3 u_player_position;
uniform highp float u_view_range;

in highp vec3 v_coord;
in highp vec3 v_pivot;
in lowp vec3 v_color;
in lowp float v_random;

layout(location = 0) out lowp vec4 frag_color;

lowp float calculate_level(
	lowp float value, 
	lowp float minimum,
	lowp float range,
	lowp int levels)
{
	lowp float flevels = float(levels);
	value = floor(value * flevels) / flevels;

	return value * range + minimum;
}

void main()
{
	highp vec3 normal = normalize(v_coord - v_pivot);
	
	lowp float saturation = calculate_level(v_random, 0.3, 0.3, 3);
	
	lowp vec3 color = vec3(1.0, 1.0, 1.0);
	color.rgb *= saturation;
	color.rgb *= calculate_diffuse_light(v_coord, normal,
										 u_player_position, 
										 u_view_range);

	frag_color = vec4(color, 1.0);
}
)";

class rotation_matrices
{
  public:
	rotation_matrices()
	{
		for (int i = 0; i < TRIANGLES; i++)
		{
			_array[i] = glm::rotate(glm::mat4(1),
									-PI / QUALITY * i,
									glm::vec3(0, 0, 1));
		}
	}

	const glm::mat4 &operator[](int i) const
	{ return _array[i % TRIANGLES]; }

  private:
	glm::mat4 _array[TRIANGLES];
};

class texture_data_set
{
  public:
	constexpr static int SIZE = RANDOM_DATA_SETS * QUALITY;

	texture_data_set()
	{
		for (GLfloat &f : _data)
			f = static_cast<float>(std::rand()) / RAND_MAX;
	}

	const GLfloat *get() const 
	{ return _data; }

  private:
	GLfloat _data[SIZE];
};

const rotation_matrices offsets;
const texture_data_set texture_data;

  template<typename type>
class data_set_array
{
  public:
	constexpr static int size = FRAMES_PER_CALL * 8;

	data_set_array()
	{ reset(); }

	void reset()
	{ index = 0; }

	void append(const type &value)
	{ data[index++] = value; }

	type data[size];
	int index;
};

LOCAL_END

class tunnel::shader : public dynamic_lighting::shader
{
  public:
	void resolve() override
	{
		dynamic_lighting::shader::resolve();

		u_models = glGetUniformLocation(program, "u_models");
		u_mvps = glGetUniformLocation(program, "u_mvps");
		u_hash_ids = glGetUniformLocation(program, "u_hash_ids");
		u_material_color = glGetUniformLocation(program, "u_material_color");
		u_player_position = glGetUniformLocation(program, "u_player_position");
		u_view_range = glGetUniformLocation(program, "u_view_range");
		u_randoms = glGetUniformLocation(program, "u_randoms");

		glUseProgram(program);
		glUniform1fv(u_randoms, RANDOM_DATA_SETS * QUALITY, 
					 texture_data.get());
	}

	GLuint u_models;
	GLuint u_mvps;
	GLuint u_hash_ids;
	GLuint u_material_color;
	GLuint u_player_position;
	GLuint u_view_range;
	GLuint u_randoms;
};

static dynamic_lighting::shader *make_shader()
{ return new tunnel::shader; }

static dynamic_lighting *create_lighting(context_object *parent)
{
	return dynamic_lighting::build()
		.vertex_source(1, &vertex_code)
		.fragment_source(1, &fragment_code)
		.shader_allocator(make_shader)
		.parent(parent)
		.done();
}


const float tunnel::radius = 4;
const float tunnel::segment_length = radius * 2 * PI / QUALITY;

/*
 * tunnel class
 */

enum
{
	COORD_VBO,
	COORD_IBO,
	BUFFER_COUNT,
};

tunnel::tunnel(context *context)
	: context_part(context)
	, _lighting(create_lighting(this))
	, _buffers(new GLuint[BUFFER_COUNT])
{
	glGenBuffers(BUFFER_COUNT, _buffers);
	_gen_coord_data();
}

tunnel::~tunnel()
{
	glDeleteBuffers(BUFFER_COUNT, _buffers);

	delete _buffers;
}

void tunnel::draw()
{
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);

	glBindBuffer(GL_ARRAY_BUFFER, _buffers[COORD_VBO]);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _buffers[COORD_IBO]);

	shader *sh = static_cast<shader *>(get_lighting()->use_program());

	glEnableVertexAttribArray(a_coord);

	glVertexAttribPointer(a_coord, 2, GL_FLOAT, GL_FALSE, 
						  sizeof(GLfloat) * 2, nullptr);

	glUniform3f(sh->u_material_color, 
				1.0, 1.0, 1.0);

	glUniform3fv(sh->u_player_position, 1, 
				 glm::value_ptr(get_part<player>()->get_matrix() * glm::vec4(0, 0, 0, 1)));

	glUniform1f(sh->u_view_range, 
				get_part<player>()->get_view_range());

	path_frame *path_frame = get_part<path_line>()->get_first_path_frame();

	while (path_frame && path_frame->get_next())
	{
		int segments = 0;

		data_set_array<glm::mat4> models;
		data_set_array<glm::mat4> mvps;
		data_set_array<GLint> hash_ids;

		if (path_frame->get_prev())
			path_frame = path_frame->get_prev();

		while (path_frame && segments < FRAMES_PER_CALL - 1)
		{
			glm::mat4 offset = offsets[path_frame->get_index()];

			glm::mat4 matrix = path_frame->get_matrix() * offset;

			models.append(matrix);
			matrix = get_part<world>()->make_mvp_matrix(matrix);
			mvps.append(matrix);

			hash_ids.append(path_frame->get_hash_id() % RANDOM_DATA_SETS);



			path_frame = path_frame->get_next();
			segments++;
		}

		int data_size = (segments + 1);

		glUniformMatrix4fv(sh->u_models, data_size, GL_FALSE, 
						   reinterpret_cast<const GLfloat *>(models.data));
		glUniformMatrix4fv(sh->u_mvps, data_size, GL_FALSE,
						   reinterpret_cast<const GLfloat *>(mvps.data));
		glUniform1iv(sh->u_hash_ids, data_size,
					  reinterpret_cast<const GLint *>(hash_ids.data));

		glDrawElementsInstanced(GL_TRIANGLES, TRIANGLES * 3,
								GL_UNSIGNED_BYTE, nullptr, segments - 1);
	}

	glDisableVertexAttribArray(a_coord);
}

dynamic_lighting *tunnel::get_lighting() const
{ return _lighting; }

void tunnel::_gen_coord_data()
{
	std::vector<GLfloat> vdata;
	std::vector<GLubyte> idata;

	auto build_vertex = [&vdata](int i) -> void {
		i %= TRIANGLES;
		float angle = PI * 2 * i / QUALITY / 2;

		float x = sinf(angle) * radius;
		float y = cosf(angle) * radius;

		vdata.push_back(x);
		vdata.push_back(y);
	};

	auto push_index = [&idata](GLubyte id) -> void {
		idata.push_back(id % TRIANGLES);
	};

	for (int i = 0; i < QUALITY; i++)
	{
		build_vertex(i * 2);
		build_vertex(i * 2);

		push_index(i * 2);
		push_index(i * 2 + 1);
		push_index(i * 2 + 2);

		push_index(i * 2 + 1);
		push_index(i * 2 + 3);
		push_index(i * 2 + 2);
	}

	glBindBuffer(GL_ARRAY_BUFFER, _buffers[COORD_VBO]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * vdata.size(),
				 vdata.data(), GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _buffers[COORD_IBO]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLubyte) * idata.size(),
				 idata.data(), GL_STATIC_DRAW);
}
