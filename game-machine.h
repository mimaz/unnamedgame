/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#ifndef __GAME_MACHINE_H
#define __GAME_MACHINE_H

#include "context-part.h"

class game_machine : public context_part, public context_object::object_registry_listener
{
  public:
	class state_callback;

	game_machine(context *ctx);

	void on_object_registered(context_object *object) override;
	void on_object_unregistered(context_object *object) override;

	void switch_state(game_state state);

	game_state get_state() const;

  private:
	void _switch_menu();
	void _switch_play();
	void _set_state(game_state state);

	game_state _state;

	std::unordered_set<state_callback *> _callback_set;
};

class game_machine::state_callback
{
  public:
	virtual void on_game_state_changed(game_state state) = 0;
};

#endif
