/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#ifndef __MENU_BUTTON_H
#define __MENU_BUTTON_H

#include "surface-item.h"
#include "menu-button-callback.h"

class buffered_text;

class menu_button : public surface_item
{
  public:
	menu_button(context_object *parent);
	~menu_button();

	void on_geometry_changed() override;
	void on_touch_event(touch_event event) override;

	void draw() override;

	void register_callback(menu_button_callback *cb);
	void unregister_callback(menu_button_callback *cb);

	void set_text(const std::string &text);
	void set_text_size(float size);
	void set_id(int id);

	std::string get_text() const;
	float get_text_size() const;
	int get_id() const;

  private:
	buffered_text * const _buffered_text;

	int _id;
	std::unordered_set<menu_button_callback *> _callback_set;

	glm::vec2 _center[2];
	int _path_frames_pressed[2];
	int _path_frames_released[2];
	int _mode[2];
	int _touch_id;
	float _wibe_angle;
};

#endif
