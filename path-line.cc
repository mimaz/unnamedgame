/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#include <std.h>

#include "path-line.h"
#include "path-frame.h"
#include "world.h"
#include "tunnel.h"
#include "stuff-manager.h"

/*
 * path_line class
 */

path_line::path_line(context *context)
	: context_part(context)
	, _first_path_frame(nullptr)
	, _last_path_frame(nullptr)
{}

void path_line::clear_path_line()
{
	path_frame *fr = _first_path_frame;

	while (fr)
	{
		path_frame *next = fr->get_next();
		delete_object(fr);
		fr = next;
	}
}

void path_line::push_path_frame_back(path_frame *fr)
{ 
	if (_first_path_frame || _last_path_frame)
	{
		assert(_first_path_frame && _last_path_frame);

		_last_path_frame->insert_back(fr);
		_last_path_frame = fr;
	}
	else
	{
		_set_initial_path_frame(fr);
	}

	get_part<stuff_manager>()->on_push_path_frame_back(fr);
}

path_frame *path_line::pop_path_frame_front()
{
	path_frame *removed = _first_path_frame;
	get_part<stuff_manager>()->on_pop_path_frame_front(removed);

	_first_path_frame = removed->get_next();

	removed->cut_out();

	return removed;
}

void path_line::emplace_path_frame_back(const glm::mat4 &matrix)
{
	path_frame *last = get_last_path_frame();

	glm::mat4 frmat;
	float location;
	int index;

	if (last)
	{
		glm::vec3 offset = last->get_matrix() * glm::vec4(0, 0, 1, 0)
			* tunnel::segment_length;

		glm::vec3 last_position = last->get_matrix() * glm::vec4(0, 0, 0, 1);

		frmat = glm::translate(frmat, last_position);
		frmat *= matrix;
		frmat = glm::translate(frmat, offset);
		frmat = glm::translate(frmat, -last_position);
		frmat *= last->get_matrix();

		location = last->get_location() + tunnel::segment_length;
		index = last->get_index() + 1;
	}
	else
	{
		frmat = matrix;

		location = 0;
		index = 1000000;
	}

	path_frame *fr = create_object<path_frame>(this);

	fr->set_location(location);
	fr->set_index(index);
	fr->set_matrix(frmat);

	push_path_frame_back(fr);
}

void path_line::delete_path_frame_front()
{
	delete_object(pop_path_frame_front());
}

path_frame *path_line::find_path_frame_by_location(float location) const
{
	if (location < get_start_location()
			|| location > get_end_location())
	{
		return nullptr;
	}

	path_frame *iter = get_first_path_frame();

	while (iter->get_next())
	{
		path_frame *next = iter->get_next();

		if (location >= iter->get_location()
				&& location <= next->get_location())
		{
			return std::abs(location - iter->get_location()) 
				< std::abs(location - next->get_location())
				? iter
				: next;
		}

		iter = next;
	}

	// TODO
	abort();

	return nullptr;
}

path_frame *path_line::get_first_path_frame() const
{ return _first_path_frame; }

path_frame *path_line::get_last_path_frame() const
{ return _last_path_frame; }

float path_line::get_start_location() const 
{ return get_first_path_frame()->get_location(); }

float path_line::get_end_location() const
{ return get_last_path_frame()->get_location(); }

void path_line::_set_initial_path_frame(path_frame *fr)
{
	assert(_first_path_frame == nullptr && _last_path_frame == nullptr);

	_first_path_frame = fr;
	_last_path_frame = fr;
}
