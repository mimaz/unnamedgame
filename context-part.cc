/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#include <std.h>

#include "context-part.h"
#include "context.h"

context_part::context_part(context *ctx)
	: context_object(ctx)
{}

void context_part::on_window_resize(int width, int height)
{}

void context_part::on_touch_event(touch_event event)
{}
