/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#include <std.h>

#include "light-group.h"
#include "light-source.h"

light_group::light_group(context_object *parent)
	: context_object(parent)
{}

light_group::~light_group()
{}

void light_group::register_light(const light_source *light)
{ _light_set.insert(light); }

void light_group::unregister_light(const light_source *light)
{ _light_set.erase(light); }

int light_group::get_light_count() const
{ return _light_set.size(); }

int light_group::read_all_sources(const light_source **array) const
{
	const light_source **ptr = array;

	for (const light_source *src : _light_set)
		*ptr++ = src;

	return ptr - array;
}

int light_group::read_reachable_sources(glm::vec3 point,
										const light_source **array) const
{
	const light_source **ptr = array;

	for (const light_source *src : _light_set)
	{
		glm::vec3 diff = point - src->get_position();
		float dist = glm::length(diff);

		if (dist < src->get_range())
			*ptr++ = src;
	}

	return ptr - array;
}
