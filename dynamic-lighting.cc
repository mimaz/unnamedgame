/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#include <std.h>

#include "dynamic-lighting.h"
#include "light-source.h"
#include "light-group.h"

static GLchar logmsg[256];

static const GLchar * const light_count_code = 
"#version 310 es\n"
"const int c_light_count = ";

static const GLchar * const light_calculation_code =
R"(
uniform highp vec3 u_light_positions[c_light_count];
uniform lowp vec3 u_light_colors[c_light_count];
uniform highp float u_light_ranges[c_light_count];

lowp vec3 calculate_diffuse_light
(
	highp vec3 coord,
	lowp vec3 normal,
	highp vec3 camera,
	highp float view_range
)
{
	lowp vec3 color = vec3(0.0, 0.0, 0.0);

	highp vec3 camera_vector = camera - coord;
	highp float camera_dist = length(camera_vector);

	if (view_range < camera_dist)
		return color;

	for (int i = 0; i < max(c_light_count, 1); i++)
	{
		highp vec3 light_vector = coord - u_light_positions[i];

		highp float squared_light_dist = light_vector.x * light_vector.x
				+ light_vector.y * light_vector.y
				+ light_vector.z * light_vector.z;

		if (squared_light_dist >= u_light_ranges[i] * u_light_ranges[i])
			continue;

		highp float light_dist = sqrt(squared_light_dist);
	
		lowp float cosine = dot(light_vector, normal) / length(light_vector) / length(normal);

		if (cosine < 0.0)
			continue;

		lowp float brightness = (u_light_ranges[i] - light_dist) / u_light_ranges[i];

		brightness = 1.0 - brightness;
		brightness *= brightness;
		brightness = 1.0 - brightness;

		lowp float factor = cosine * brightness;
		color += u_light_colors[i] * factor;
	}

	lowp float fog = (view_range - camera_dist) / view_range;
	color *= fog;

	return color;
}
)";

/*
 * dynamic_lighting
 */

dynamic_lighting::builder dynamic_lighting::build()
{ return builder(); }

dynamic_lighting::dynamic_lighting(context_object *parent)
	: context_object(parent)
{
	for (shader *&sh : _shaders)
		sh = nullptr;
}

dynamic_lighting::~dynamic_lighting()
{
	glDeleteShader(_vshader);

	for (shader *sh : _shaders)
		delete sh;
}

void dynamic_lighting::set_light_group(const light_group *group)
{ _light_group = group; }

dynamic_lighting::shader *dynamic_lighting::use_program()
{
	return use_program(get_light_group());
}

dynamic_lighting::shader *dynamic_lighting::use_program(const light_group *group)
{
	assert(group);

	int lightno = group->get_light_count();
	const light_source *light_sources[lightno];

	lightno = group->read_all_sources(light_sources);

	return _use_program(lightno, light_sources);
}

dynamic_lighting::shader *dynamic_lighting::use_program(glm::vec3 position)
{
	return use_program(position, get_light_group());
}

dynamic_lighting::shader *dynamic_lighting::use_program(glm::vec3 position,
														const light_group *group)
{
	assert(group);

	int lightno = get_light_group()->get_light_count();
	const light_source *light_sources[group->get_light_count()];

	lightno = group->read_reachable_sources(position, light_sources);

	return _use_program(lightno, light_sources);
}

const light_group *dynamic_lighting::get_light_group() const
{ return _light_group; }

dynamic_lighting::shader *dynamic_lighting::_get_shader(int count)
{
	if (count < 1)
		count = 1;

	assert(count < static_cast<int>(_shaders.size()));

	if (_shaders[count] == nullptr)
	{
		_shaders[count] = _shader_allocator();

		_shaders[count]->build(_fragment_sources.size(),
							   _fragment_sources.data(),
							   _vshader,
							   count);
	}

	return _shaders[count];
}

dynamic_lighting::shader *dynamic_lighting::_use_program(int light_count, 
														 const light_source **light_array)
{
	shader *sh = _get_shader(light_count);
	glUseProgram(sh->program);

	glm::vec3 positions[light_count];
	glm::vec3 colors[light_count];
	GLfloat ranges[light_count];


	for (int i = 0; i < light_count; i++)
	{
		const light_source *src = light_array[i];

		positions[i] = src->get_position();
		colors[i] = src->get_real_color();
		ranges[i] = src->get_range();
	}

	glUniform3fv(sh->u_light_positions, light_count, 
				 reinterpret_cast<const GLfloat *>(positions));
	glUniform3fv(sh->u_light_colors, light_count, 
				 reinterpret_cast<const GLfloat *>(colors));
	glUniform1fv(sh->u_light_ranges, light_count, 
				 reinterpret_cast<const GLfloat *>(ranges));

	return sh;
}

/*
 * dynamic_lighting::shader
 */

typedef dynamic_lighting::shader shader;

static void fragment_source(GLuint fcnt, 
							const GLchar * const * fsrc,
							GLuint fsh, 
							int count)
{
	assert(count < 20);
	assert(count >= 0);


	const int len0 = strlen(light_count_code) + 2;
	GLchar appropriate_light_count_code[len0] = { 0 };

	GLchar suffix[4];
	GLchar *ptr = suffix;

	if (count > 9)
		*ptr++ = count / 10 + '0';

	*ptr++ = count % 10 + '0';
	*ptr++ = ';';
	*ptr++ = 0;

	strcat(appropriate_light_count_code, light_count_code);
	strcat(appropriate_light_count_code, suffix);

	const GLchar *sources[fcnt + 2] = {
		appropriate_light_count_code,
		light_calculation_code
	};

	for (int i = 0; i < static_cast<int>(fcnt); i++)
		sources[i + 2] = fsrc[i];

	glShaderSource(fsh, fcnt + 2, sources, nullptr);
}

shader::~shader()
{
	glDeleteProgram(program);
}

void shader::build(GLuint fcnt,
				   const GLchar * const * fsrc,
				   GLuint vsh,
				   int count)
{
	GLuint fsh = glCreateShader(GL_FRAGMENT_SHADER);
	fragment_source(fcnt, fsrc, fsh, count);
	glCompileShader(fsh);

	GLint ok;
	glGetShaderiv(fsh, GL_COMPILE_STATUS, &ok);

	if (! ok)
	{
		glGetShaderInfoLog(fsh, 256, nullptr, logmsg);
		std::cerr << "compilling fragment shader for dynamic lighting failed: " 
			<< logmsg << std::endl;
		abort();
	}


	program = glCreateProgram();
	glAttachShader(program, vsh);
	glAttachShader(program, fsh);
	glLinkProgram(program);

	glGetProgramiv(program, GL_LINK_STATUS, &ok);

	if (! ok)
	{
		glGetProgramInfoLog(program, 256, nullptr, logmsg);
		std::cerr << "linking program for dynamic lighting failed: "
			<< logmsg << std::endl;

		abort();
	}

	glDeleteShader(fsh);

	resolve();
}

void shader::resolve()
{
	u_light_positions = glGetUniformLocation(program, "u_light_positions");
	u_light_colors = glGetUniformLocation(program, "u_light_colors");
	u_light_ranges = glGetUniformLocation(program, "u_light_ranges");
}

/*
 * dynamic_lighting::builder  class
 */

typedef dynamic_lighting::builder builder;

static dynamic_lighting::shader *make_shader()
{
	return new dynamic_lighting::shader;
}

builder::builder()
	: _vcnt(0)
	, _vsrc(nullptr)
	, _fcnt(0)
	, _fsrc(nullptr)
	, _allocator(make_shader)
	, _light_group(nullptr)
	, _parent(nullptr)
{}

builder &builder::vertex_source(GLuint vcnt,
								const GLchar * const * vsrc)
{
	_vcnt = vcnt;
	_vsrc = vsrc;
	return *this;
}

builder &builder::fragment_source(GLuint fcnt,
								  const GLchar * const * fsrc)
{
	_fcnt = fcnt;
	_fsrc = fsrc;
	return *this;
}

builder &builder::shader_allocator(shader *(*allocator)(void))
{
	_allocator = allocator;
	return *this;
}

builder &builder::light_source_group(const light_group *group)
{
	_light_group = group;
	return *this;
}

builder &builder::parent(context_object *parent)
{
	_parent = parent;
	return *this;
}

dynamic_lighting *builder::done()
{
	assert(_parent);

	dynamic_lighting *lighting = _parent->create_object<dynamic_lighting>(_parent);

	lighting->_shader_allocator = _allocator;
	lighting->_light_group = _light_group;

	if (_vcnt == 0 || _fcnt == 0)
	{
		std::cerr << "dynamic_lighting require vertex "
			"and fragment source code!" << std::endl;
		abort();
	}

	for (GLuint i = 0; i < _fcnt; i++)
		lighting->_fragment_sources.push_back(_fsrc[i]);

	GLuint vsh = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vsh, _vcnt, _vsrc, nullptr);
	glCompileShader(vsh);

	GLint ok;
	glGetShaderiv(vsh, GL_COMPILE_STATUS, &ok);

	if (! ok)
	{
		glGetShaderInfoLog(vsh, 256, nullptr, logmsg);
		std::cerr << "compilling dynamic_lighting vertex shader failed! "
			<< logmsg << std::endl;
		abort();
	}

	lighting->_vshader = vsh;

	return lighting;
}
