/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#include <std.h>

#include "main-menu.h"
#include "context.h"
#include "surface.h"
#include "menu-button.h"
#include "texture-effect.h"

LOCAL_BEGIN

const GLchar * const story_message =
"FAR FAR AWAY IN\n"
"SWIECIECHOWA GALAXY\n"
"SOMEBODY HAVE MADE A GAME\n"
"AND NOW YOU HAVE TO RESCUE WHOLE\n"
"WORLD BY REACHING THE END OF INFINITE\n"
"TUNNEL, REPORTING BUGS AND GIVING FEEDBACK...\n"
;

enum button_id
{
	button_start,
	button_story,
	button_achievements,
	button_leaderboard,
};

LOCAL_END

/*
 * main_menu class
 */

main_menu::main_menu(context *context)
	: context_part(context)
	, _start_button(create_object<menu_button>(this))
	, _story_button(create_object<menu_button>(this))
	, _achievements_button(create_object<menu_button>(this))
	, _leaderboards_button(create_object<menu_button>(this))
	, _exposure(1.0)
	, _target_exposure(1.0)
{}

main_menu::~main_menu()
{
	glDeleteTextures(1, &_texture);
	glDeleteFramebuffers(1, &_texture);
}

void main_menu::on_create()
{
	context_part::on_create();

	glGenTextures(1, &_texture);
	glGenFramebuffers(1, &_path_framebuffer);

	glBindTexture(GL_TEXTURE_2D, _texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0, GL_RGBA, 
				 GL_UNSIGNED_BYTE, 0);

	glBindFramebuffer(GL_FRAMEBUFFER, _path_framebuffer);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, 
						   GL_TEXTURE_2D, _texture, 0);

	assert(glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void main_menu::on_flag_changed(int flag, bool value)
{
	context_part::on_flag_changed(flag, value);

	if (flag == flag_varying_exposure
			&& ! value
			&& get_exposure() < 0.001)
	{
	}
}

void main_menu::draw()
{
	glBindFramebuffer(GL_FRAMEBUFFER, _path_framebuffer);

	_start_button->draw();
	_story_button->draw();
	_achievements_button->draw();
	_leaderboards_button->draw();

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glm::vec3 size = glm::vec3(
			get_context()->get_window_width(),
			get_context()->get_window_height(),
			1);

	glm::mat4 matrix = glm::scale(glm::mat4(1), size);
	matrix = get_part<surface>()->make_mvp(matrix);

	get_part<texture_effect>()->draw_with_blur_effect(_texture, matrix, get_exposure());
}

void main_menu::preprocess()
{
	context_part::preprocess();

	if (is_flag_set(flag_varying_exposure))
	{
		const float step = 1.0 / 0.4 / 60.0;

		float diff = get_target_exposure() - get_exposure();

		if (diff > 0.0)
			_exposure += step;
		else
			_exposure -= step;

		diff *= get_target_exposure() - get_exposure();

		if (diff < 0.0)
		{
			_exposure = get_target_exposure();
			reset_flag(flag_varying_exposure);
		}
	}
}

void main_menu::on_window_resize(int width, int height)
{
	context_part::on_window_resize(width, height);

	float button_width = width / 3.0;
	float button_height = height / 5.0;
	float button_text_size = height / 26.0;
	float left_offset = -width / 2 + button_width * 3 / 4;

	_start_button->set_text("START!");
	_start_button->set_x(-left_offset);
	_start_button->set_width(button_width);
	_start_button->set_height(button_height * 3);
	_start_button->set_text_size(button_text_size * 2);
	_start_button->set_id(button_start);
	_start_button->register_callback(this);

	_story_button->set_text("THE\nSTORY");
	_story_button->set_x(left_offset);
	_story_button->set_y(button_height * 4 / 3);
	_story_button->set_width(button_width);
	_story_button->set_height(button_height);
	_story_button->set_text_size(button_text_size);
	_story_button->set_id(button_story);
	_story_button->register_callback(this);

	_achievements_button->set_text("ACHIEVEMENTS");
	_achievements_button->set_x(left_offset);
	_achievements_button->set_width(button_width);
	_achievements_button->set_height(button_height);
	_achievements_button->set_text_size(button_text_size);
	_achievements_button->set_id(button_achievements);
	_achievements_button->register_callback(this);

	_leaderboards_button->set_text("LEADERBOARDS");
	_leaderboards_button->set_x(left_offset);
	_leaderboards_button->set_y(-button_height * 4 / 3);
	_leaderboards_button->set_width(button_width);
	_leaderboards_button->set_height(button_height);
	_leaderboards_button->set_text_size(button_text_size);
	_leaderboards_button->set_id(button_leaderboard);
	_leaderboards_button->register_callback(this);

	glBindTexture(GL_TEXTURE_2D, _texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA,
				 GL_UNSIGNED_BYTE, 0);
}

void main_menu::on_menu_button_clicked(menu_button *button)
{
	switch (button->get_id())
	{
	  case button_start:
		break;
	}
}

void main_menu::set_target_exposure(float exposure)
{ 
	_target_exposure = exposure; 
	set_flag(flag_varying_exposure);
}

GLuint main_menu::get_texture_handle() const
{ return _texture; }

float main_menu::get_exposure() const
{ return _exposure; }

float main_menu::get_target_exposure() const
{ return _target_exposure; }
