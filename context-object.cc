/*
 * Created by Mieszko Mazurek <mimaz@gmx.com>
 */

#include <std.h>

#include "context-object.h"
#include "context.h"

int context_object::created;
int context_object::destroyed;

context_object::context_object(context *context)
	: context_object(context, context)
{}

context_object::context_object(context_object *parent)
	: context_object(parent->get_context(), parent)
{}

context_object::context_object(context *context, context_object *parent)
	: _context(context)
	, _parent(parent)
{
	if (get_context() == this)
		get_context()->set_flag(context::flag_creatable_object);

	if (! get_context()->is_flag_set(context::flag_creatable_object))
	{
		std::cerr << "context_object must be created through context!" << std::endl;
		abort();
	}

	get_context()->reset_flag(context::flag_creatable_object);
}

context_object::~context_object()
{
	if (get_context() == this)
		get_context()->set_flag(context::flag_deletable_object);

	if (! get_context()->is_flag_set(context::flag_deletable_object))
	{
		std::cerr << "context_object must be deleted through context!" << std::endl;
		abort();
	}

	get_context()->reset_flag(context::flag_deletable_object);
}

void context_object::on_create()
{
	get_context()->register_object(this);

	if (get_parent() 
			&& get_parent() != this)
	{
		get_parent()->register_child(this);
	}
}

void context_object::on_destroy()
{
	if (get_parent())
		get_parent()->unregister_child(this);
	
	// delete children
	std::vector<context_object *> removable(
			_child_set.begin(), _child_set.end());

	for (context_object *child : removable)
	{
		assert(child != this);
		delete_object(child);
	}

	assert(_child_set.empty());

	get_context()->unregister_object(this);
}

void context_object::preprocess()
{
	for (context_object *object : _child_set)
		object->preprocess();
}

void context_object::on_flag_changed(int flag, bool value)
{}

void context_object::register_child(context_object *child)
{
	assert(child->get_parent() == this);

	_child_set.insert(child);
}

void context_object::unregister_child(context_object *child)
{
	assert(child->get_parent() == this);

	_child_set.erase(child);
}

void context_object::set_flag(int flag)
{
	if (! is_flag_set(flag))
	{
		_flags |= 1 << flag;
		on_flag_changed(flag, true);
	}
}

void context_object::reset_flag(int flag)
{
	if (is_flag_set(flag))
	{
		_flags &= ~(1 << flag);
		on_flag_changed(flag, false);
	}
}

void context_object::write_flag(int flag, bool value)
{
	if (value)
		set_flag(flag);
	else
		reset_flag(flag);
}

bool context_object::is_flag_set(int flag) const
{ return _flags & (1 << flag); }

context *context_object::get_context() const
{ return _context; }

context_object *context_object::get_parent() const
{ return _parent; }

std::size_t context_object::get_hash() const
{
	return typeid(*this).hash_code();
}

void context_object::delete_object(context_object *object)
{ get_context()->delete_object(object); }

context_part *context_object::_get_part_ptr(const std::type_info &tinfo)
{ return get_context()->get_part_ptr(tinfo); }

void context_object::_mark_creatable_object()
{
	get_context()->set_flag(context::flag_creatable_object);
}
